const mix = require('laravel-mix');

const publicPath = './public';

mix.setPublicPath(publicPath);
mix.options({ processCssUrls: false });

let countryFlags = [
    'gb', 'ua', 'ru'
];

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/admin/admin.js', 'js/admin');

mix.sass('resources/sass/admin/admin.scss', 'css/admin');

mix.copyDirectory('node_modules/material-design-icons-iconfont/dist/fonts', publicPath + '/fonts/vendor/material-design-icons');

//copy flag icons
for(let i of countryFlags) {
    const flag = i + '.svg';
    mix.copy('node_modules/flag-icon-css/flags/1x1/' + flag, publicPath + '/flags/1x1/' + flag);
    mix.copy('node_modules/flag-icon-css/flags/4x3/' + flag, publicPath + '/flags/4x3/' + flag);
}

mix.js('resources/js/app.js', 'js')
   .sass('resources/sass/app.scss', 'css');
