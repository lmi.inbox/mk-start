(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? module.exports = factory() :
        typeof define === 'function' && define.amd ? define(factory) :
            (global.vuei18nLocales = factory());
}(this, (function () { 'use strict';
    return {
    "en": {
        "auth": {
            "failed": "These credentials do not match our records.",
            "throttle": "Too many login attempts. Please try again in {seconds} seconds."
        },
        "messages": {
            "mail": {
                "subcopy": "If you’re having trouble clicking the \"{actionText}\" button, copy and paste the URL below into your web browser: [{actionUrl}]({actionUrl})"
            },
            "info": {
                "webserver": "Web Server",
                "apache_version": "Apache Version",
                "apache_version_number": "Apache Version Number",
                "php_version": "PHP Version",
                "php_built_on": "PHP Built On",
                "sapi_name": "WebServer to PHP Interface",
                "user_agent": "User Agent",
                "db_server": "Database Server",
                "db_version": "Database Version",
                "db_collation": "Database Collation",
                "db_connection_collation": "Database Connection Collation"
            }
        },
        "pagination": {
            "previous": "« Previous",
            "next": "Next »"
        },
        "passwords": {
            "password": "Passwords must be at least six characters and match the confirmation.",
            "reset": "Your password has been reset!",
            "sent": "We have e-mailed your password reset link!",
            "token": "This password reset token is invalid.",
            "user": "We can't find a user with that e-mail address.",
            "mail_text": "You are receiving this email because we received a password reset request for your account.",
            "mail_note": "If you did not request a password reset, no further action is required."
        },
        "validation": {
            "accepted": "The {attribute} must be accepted.",
            "active_url": "The {attribute} is not a valid URL.",
            "after": "The {attribute} must be a date after {date}.",
            "after_or_equal": "The {attribute} must be a date after or equal to {date}.",
            "alpha": "The {attribute} may only contain letters.",
            "alpha_dash": "The {attribute} may only contain letters, numbers, dashes and underscores.",
            "alpha_num": "The {attribute} may only contain letters and numbers.",
            "array": "The {attribute} must be an array.",
            "before": "The {attribute} must be a date before {date}.",
            "before_or_equal": "The {attribute} must be a date before or equal to {date}.",
            "between": {
                "numeric": "The {attribute} must be between {min} and {max}.",
                "file": "The {attribute} must be between {min} and {max} kilobytes.",
                "string": "The {attribute} must be between {min} and {max} characters.",
                "array": "The {attribute} must have between {min} and {max} items."
            },
            "boolean": "The {attribute} field must be true or false.",
            "confirmed": "The {attribute} confirmation does not match.",
            "date": "The {attribute} is not a valid date.",
            "date_format": "The {attribute} does not match the format {format}.",
            "different": "The {attribute} and {other} must be different.",
            "digits": "The {attribute} must be {digits} digits.",
            "digits_between": "The {attribute} must be between {min} and {max} digits.",
            "dimensions": "The {attribute} has invalid image dimensions.",
            "distinct": "The {attribute} field has a duplicate value.",
            "email": "The {attribute} must be a valid email address.",
            "exists": "The selected {attribute} is invalid.",
            "file": "The {attribute} must be a file.",
            "filled": "The {attribute} field must have a value.",
            "gt": {
                "numeric": "The {attribute} must be greater than {value}.",
                "file": "The {attribute} must be greater than {value} kilobytes.",
                "string": "The {attribute} must be greater than {value} characters.",
                "array": "The {attribute} must have more than {value} items."
            },
            "gte": {
                "numeric": "The {attribute} must be greater than or equal {value}.",
                "file": "The {attribute} must be greater than or equal {value} kilobytes.",
                "string": "The {attribute} must be greater than or equal {value} characters.",
                "array": "The {attribute} must have {value} items or more."
            },
            "image": "The {attribute} must be an image.",
            "in": "The selected {attribute} is invalid.",
            "in_array": "The {attribute} field does not exist in {other}.",
            "integer": "The {attribute} must be an integer.",
            "ip": "The {attribute} must be a valid IP address.",
            "ipv4": "The {attribute} must be a valid IPv4 address.",
            "ipv6": "The {attribute} must be a valid IPv6 address.",
            "json": "The {attribute} must be a valid JSON string.",
            "lt": {
                "numeric": "The {attribute} must be less than {value}.",
                "file": "The {attribute} must be less than {value} kilobytes.",
                "string": "The {attribute} must be less than {value} characters.",
                "array": "The {attribute} must have less than {value} items."
            },
            "lte": {
                "numeric": "The {attribute} must be less than or equal {value}.",
                "file": "The {attribute} must be less than or equal {value} kilobytes.",
                "string": "The {attribute} must be less than or equal {value} characters.",
                "array": "The {attribute} must not have more than {value} items."
            },
            "max": {
                "numeric": "The {attribute} may not be greater than {max}.",
                "file": "The {attribute} may not be greater than {max} kilobytes.",
                "string": "The {attribute} may not be greater than {max} characters.",
                "array": "The {attribute} may not have more than {max} items."
            },
            "mimes": "The {attribute} must be a file of type: {values}.",
            "mimetypes": "The {attribute} must be a file of type: {values}.",
            "min": {
                "numeric": "The {attribute} must be at least {min}.",
                "file": "The {attribute} must be at least {min} kilobytes.",
                "string": "The {attribute} must be at least {min} characters.",
                "array": "The {attribute} must have at least {min} items."
            },
            "not_in": "The selected {attribute} is invalid.",
            "not_regex": "The {attribute} format is invalid.",
            "numeric": "The {attribute} must be a number.",
            "present": "The {attribute} field must be present.",
            "regex": "The {attribute} format is invalid.",
            "required": "The {attribute} field is required.",
            "required_if": "The {attribute} field is required when {other} is {value}.",
            "required_unless": "The {attribute} field is required unless {other} is in {values}.",
            "required_with": "The {attribute} field is required when {values} is present.",
            "required_with_all": "The {attribute} field is required when {values} is present.",
            "required_without": "The {attribute} field is required when {values} is not present.",
            "required_without_all": "The {attribute} field is required when none of {values} are present.",
            "same": "The {attribute} and {other} must match.",
            "size": {
                "numeric": "The {attribute} must be {size}.",
                "file": "The {attribute} must be {size} kilobytes.",
                "string": "The {attribute} must be {size} characters.",
                "array": "The {attribute} must contain {size} items."
            },
            "string": "The {attribute} must be a string.",
            "timezone": "The {attribute} must be a valid zone.",
            "unique": "The {attribute} has already been taken.",
            "uploaded": "The {attribute} failed to upload.",
            "url": "The {attribute} format is invalid.",
            "custom": {
                "attribute-name": {
                    "rule-name": "custom-message"
                }
            },
            "attributes": []
        }
    },
    "uk": {
        "Administration": "Адміністрування",
        "Authorization": "Авторизація",
        "Username": "Логін",
        "E-mail or Username": "E-mail або логін",
        "Login": "Увійти",
        "Password": "Пароль",
        "Remember me": "Запам'ятати",
        "Forgot your password?": "Забули свій пароль?",
        "Confirm password": "Повторити пароль",
        "Reset password": "Відновити пароль",
        "Logout": "Вийти",
        "Home": "Головна",
        "Submit": "Відправити",
        "Cancel": "Скасувати",
        "Hello": "Привіт",
        "Regards": "З повагою",
        "All rights reserved": "Всі права захищені",
        "Access denied": "Доступ заборонено",
        "Save": "Зберегти",
        "Language": "Мова",
        "Theme": "Тема",
        "Search": "Пошук",
        "No data available": "Немає даних",
        "Add": "Додати",
        "With selected": "З відміченими",
        "Menu": "Меню",
        "Reload": "Перезавантажити",
        "View site": "Переглянути сайт",
        "Edit account": "Редагувати обліковий запис",
        "Account": "Обліковий запис",
        "Console": "Консоль",
        "Command": "Команда",
        "Run": "Виконати",
        "Output": "Вивід",
        "Arguments": "Аргументи",
        "Options": "Параметри",
        "Dashboard": "Панель керування",
        "System": "Система",
        "System Information": "Інформація про систему",
        "Settings": "Налаштування",
        "Setting": "Налаштування",
        "Value": "Значення",
        "Modules": "Модулі",
        "Extensions": "Розширення",
        "Built on": "Базується на",
        "Full info": "Повна інформація",
        "Server Variables": "Змінні сервера",
        "Environment Variables": "Змінні середовища",
        "Error": "Помилка",
        "The form is invalid": "Форма невірна",
        "An error occurred": "Виникла помилка",
        "Page not found": "Сторінку не знайдено",
        "Users": "Користувачі",
        "User": "Користувач",
        "All": "Всі",
        "Selected": "Вибрано",
        "Rows per page": "Рядків на сторінку",
        "Are you sure?": "Ви впевнені?",
        "Delete": "Видалити",
        "Activate": "Активувати",
        "Deactivate": "Блокувати",
        "Name": "Ім'я",
        "Status": "Статус",
        "Edit": "Редагувати",
        "of": "з",
        "auth": {
            "failed": "Невірний логін\/пароль.",
            "throttle": "Занадто багато спроб входу в систему. Будь ласка, спробуйте ще раз через {seconds} секунд."
        },
        "messages": {
            "mail": {
                "subcopy": "Якщо у вас виникають проблеми при натисканні кнопки \"{actionText}\", скопіюйте та вставте URL нижче в свій веб-браузер: [{actionUrl}]({actionUrl})"
            },
            "info": {
                "webserver": "Веб-сервер",
                "apache_version": "Версія Apache",
                "php_version": "Версія PHP",
                "php_built_on": "Базується на PHP",
                "sapi_name": "Веб-сервер з інтерфейсом PHP",
                "user_agent": "Агент користувача (User Agent)",
                "db_server": "Сервер бази даних",
                "db_version": "Версія бази даних",
                "db_collation": "Співставлення бази даних",
                "db_connection_collation": "Підключення співставлення бази даних"
            }
        },
        "passwords": {
            "password": "Пароль повинен бути не менше шести символів і співпадати з підтвердженням.",
            "reset": "Ваш пароль було скинуто!",
            "sent": "Поcилання для скидання паролю відіслано на Ваш email!",
            "token": "Цей маркер скидання паролю не є дійсним.",
            "user": "Користувача з таким email не знайдено.",
            "mail_text": "Ви отримали цей лист, тому що ми отримали запит на зміну пароля для облікового запису. Натисніть на кнопку нижче, щоб скинути пароль:",
            "mail_note": "Якщо Ви не відправляли запит на зміну пароля, проігноруйте це повідомлення."
        },
        "validation": {
            "accepted": "Ви повинні прийняти {attribute}.",
            "active_url": "Поле {attribute} містить неправильне посилання.",
            "after": "Поле {attribute} повинно бути більшим за {date}.",
            "after_or_equal": "Поле {attribute} повинно бути більшим або рівним {date}.",
            "alpha": "Поле {attribute} може містити тільки літери.",
            "alpha_dash": "Поле {attribute} може містити тільки літери, цифри, тире і підкреслення.",
            "alpha_num": "Поле {attribute} може містити тільки літери та цифри.",
            "array": "Поле {attribute} повинно бути масивом.",
            "before": "Поле {attribute} повинно бути меншим за {date}.",
            "before_or_equal": "Поле {attribute} повинно бути меншим або рівним {date}.",
            "between": {
                "numeric": "Поле {attribute} повинно бути між {min} та {max}.",
                "file": "Розмір файлу {attribute} повинно бути не менше {min} та не більше {max} кілобайт.",
                "string": "Текст {attribute} повинен бути не менше {min} та не більше {max} символів.",
                "array": "Поле {attribute} повинно містити від {min} до {max} елементів."
            },
            "boolean": "Поле {attribute} повинно бути логічного типу.",
            "confirmed": "Підтвердження для поля {attribute} не співпадає.",
            "date": "Поле {attribute} не є датою.",
            "date_format": "Поле {attribute} не відповідає формату {format}.",
            "different": "Поля {attribute} та {other} повинні бути різними.",
            "digits": "Довжина цифрового поля {attribute} повинна дорівнювати {digits}.",
            "digits_between": "Довжина цифрового поля {attribute} повинна бути від {min} до {max}.",
            "dimensions": "Поле {attribute} містіть неприпустимі розміри зображення.",
            "distinct": "Поле {attribute} містить значення, яке дублюється.",
            "email": "Поле {attribute} повинне містити електронну адресу.",
            "exists": "Вибране для {attribute} значення не коректне.",
            "file": "Поле {attribute} пофинно містити файл.",
            "filled": "Поле {attribute} є обов'язковим для заповнення.",
            "image": "Поле {attribute} має містити зображення.",
            "in": "Вибране для {attribute} значення не коректне.",
            "in_array": "Значення поля {attribute} не міститься в {other}.",
            "integer": "Поле {attribute} повинно містити ціле число.",
            "ip": "Поле {attribute} повинно містити IP адресу.",
            "ipv4": "Поле {attribute} повинно містити IPv4 адресу.",
            "ipv6": "Поле {attribute} повинно містити IPv6 адресу.",
            "json": "Поле {attribute} повинно бути у форматі JSON.",
            "max": {
                "numeric": "Поле {attribute} повинно бути не більше {max}.",
                "file": "Файл {attribute} повинен бути не більше {max} кілобайт.",
                "string": "Текст {attribute} повинен мати довжину не більшу за {max}.",
                "array": "Поле {attribute} повинне містити не більше {max} елементів."
            },
            "mimes": "Поле {attribute} повинно містити файл одного з типів: {values}.",
            "mimetypes": "Поле {attribute} повинно бути файлом з типів: {values}.",
            "min": {
                "numeric": "Поле {attribute} повинно бути не менше {min}.",
                "file": "Файл {attribute} повинен бути не меншим {min} кілобайт.",
                "string": "Текст {attribute} повинен містити не менше {min} символів.",
                "array": "Поле {attribute} повинне містити не менше {min} елементів."
            },
            "not_in": "Вибране для {attribute} значення не коректне.",
            "numeric": "Поле {attribute} повинно містити число.",
            "present": "Поле {attribute} повинно бути присутнім.",
            "regex": "Поле {attribute} має невірний формат.",
            "required": "Поле {attribute} є обов'язковим для заповнення.",
            "required_if": "Поле {attribute} є обов'язковим для заповнення, коли {other} є рівним {value}.",
            "required_unless": "Поле {attribute} є обов'язковим для заповнення, коли {other} відрізняється від {values}",
            "required_with": "Поле {attribute} є обов'язковим для заповнення, коли {values} вказано.",
            "required_with_all": "Поле {attribute} є обов'язковим для заповнення, коли {values} вказано.",
            "required_without": "Поле {attribute} є обов'язковим для заповнення, коли {values} не вказано.",
            "required_without_all": "Поле {attribute} є обов'язковим для заповнення, коли {values} не вказано.",
            "same": "Поля {attribute} та {other} повинні співпадати.",
            "size": {
                "numeric": "Поле {attribute} повинно бути довжиною {size}.",
                "file": "Файл {attribute} повинно бути розміром {size} кілобайт.",
                "string": "Текст {attribute} повинен містити {size} символів.",
                "array": "Поле {attribute} повинно містити {size} елементів."
            },
            "string": "Поле {attribute} повинно містити текст.",
            "timezone": "Поле {attribute} повинно містити коректну часову зону.",
            "unique": "Таке значення поля {attribute} вже існує.",
            "uploaded": "Неможливо завантажити файл {attribute}.",
            "url": "Формат поля {attribute} неправильний.",
            "custom": {
                "attribute-name": {
                    "rule-name": "custom-message"
                }
            },
            "attributes": {
                "name": "Ім'я",
                "email": "E-mail",
                "password": "Пароль",
                "password_current": "Поточний пароль",
                "password_new": "Новий пароль"
            }
        }
    }
}

})));