<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/** @var $this \Illuminate\Routing\Router */

$this->get('/', function () {
    return view('welcome');
});

Auth::routes(['verify' => true]);

$this->get('/home', 'HomeController@index')->name('home')->middleware('verified');

$this->group([
    'prefix' => '/profile',
    'middleware' => 'verified'
], function() {
    /** @var $this \Illuminate\Routing\Router */
    $this->get('/', 'ProfileController@form')->name('profile');
    $this->get('/password', 'ProfileController@password')->name('profile.password');
    $this->post('/', 'ProfileController@update')->name('profile.update');
    $this->post('/password', 'ProfileController@updatePassword')->name('profile.password.update');
});
