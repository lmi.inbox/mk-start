<?php

/** @var $this \Illuminate\Routing\Router */

//App configuration
$this->get('config.json', 'ShareController@config')->name('config');

$this->get('/', 'IndexController@index')->name('home');
$this->get('menu.json', 'IndexController@menu')->name('menu.json');
$this->get('dashboard', 'DashboardController@index')->name('dashboard');

$this->get('console', 'ConsoleController@index')->name('console');
$this->get('console/commands.json', 'ConsoleController@commands')->name('console.commands');
$this->post('console', 'ConsoleController@run')->name('console.run');

$this->get('info', 'SystemController@info')->name('info');
$this->get('info.json', 'SystemController@infoData')->name('info.json');
$this->get('info.php', 'SystemController@infoPhp')->name('info.php');

\App\Services\AdminRoute::group(
    'UserController',
    'user',
    null,
    ['toggle' => true],
    function($router) {
        /** @var $router \Illuminate\Routing\Router */
        $router->post('verify', 'UserController@verify');
    }
);

$this->get('test', function() {
    return view('admin.test');
});
