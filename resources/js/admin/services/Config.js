class ConfigService {
    constructor() {
        this._config = {};
        this._loaded = false;
    }

    load() {
        if (this._loaded) {
            return false;
        }

        return axios.get('config.json')
            .then(response => {
                this._config = response.data;
                this._loaded = true;
                return response.data;
            });
    }

    get(key, defaultValue) {
        return this._getProp(key) || defaultValue || null;
    }

    _getProp(key) {
        return key.split('.').reduce((o, i) => {
            return o ? o[i] : null;
        }, this._config);
    }
}

if (!window.Config) {
    window.Config = new ConfigService();
}

let Config = window.Config;

export default Config;
