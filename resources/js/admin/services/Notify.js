class NotificationsService {

    constructor() {
        this.messages = [];
        this._inited = false;
    }

    init() {
        if (this._inited) {
            return;
        }

        this._inited = true;

        // clear hidden messages every 5 minutes
        setInterval(() => this.clearHidden(), 60000 * 5);
    }

    alert(text, type) {
        this.messages.push({
            text,
            color: this.constructor._color(type),
            show: true,
            timeout: 7000
        });
    }

    remove(message) {
        const i = this.messages.indexOf(message);

        if (i >= 0) {
            this.messages.splice(i, 1);
        }
    }

    clearHidden() {
        if (this.messages) {
            for (let i = 0; i < this.messages.length; i++) {
                if (!this.messages[i].show) {
                    this.messages.splice(i--, 1);
                }
            }
        }
    }

    static _color(type) {
        if (!type || ['success', 'info', 'error', 'warning'].indexOf(type) === -1) {
            return 'info'
        }

        return type;
    }

    error(message) {
        this.alert(message, 'error');
    }
}

const Notify = new NotificationsService();

export default Notify;
