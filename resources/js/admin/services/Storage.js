
export const Storage = {

    get(key, defaultValue = null) {
        return window.localStorage.getItem(key) || defaultValue;
    },

    put(key, value) {
        return window.localStorage.setItem(key, value);
    },

    getObject(key, defaultValue = null) {
        const result = window.localStorage.getItem(key);
        return result ? this._safeJsonParse(result) : defaultValue;
    },

    putObject(key, value) {
        return window.localStorage.setItem(key, JSON.stringify(value));
    },

    remove(key) {
        return window.localStorage.removeItem(key);
    },

    _safeJsonParse(str) {
        try {
            return JSON.parse(str);
        } catch (e) {
            return str;
        }
    }
};
