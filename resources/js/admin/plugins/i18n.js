import Vue from 'vue';
import VueI18n from 'vue-i18n';
//import Locale from './../../vue-i18n-locales.generated';

Vue.use(VueI18n);

const lang = document.documentElement.lang.substr(0, 2);

//Vue.config.lang = lang;
//Object.keys(window.vuei18nLocales).forEach(function (lang) {
//    Vue.i18n.add(lang, window.vuei18nLocales[lang]);
//});

/**Object.keys(window.vuei18nLocales).forEach(function (lang) {
    Vue.locale(lang, window.vuei18nLocales[lang]);
});**/

//const lang = document.documentElement.lang.substr(0, 2);

const i18n = new VueI18n({
    locale: lang,
    fallbackLocale: 'en',
    messages: window.vuei18nLocales//Locale
});
window.i18n = i18n;

export default i18n;
