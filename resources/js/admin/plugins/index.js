import './vuetify';
import i18n from './i18n';
import axios from './axios';

export {
    i18n,
    axios
};
