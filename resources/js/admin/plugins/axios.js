import {baseUrl, url} from "../helpers";
import Notify from "../services/Notify";
import i18n from './i18n';
import store from './../store';
import qs from 'qs';

let t = function (key, values = undefined) {
    return i18n.t(key, values);
};

const axios = require('axios');

axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

/**
 * Next we will register the CSRF Token as a common header with Axios so that
 * all outgoing HTTP requests automatically have it attached. This is just
 * a simple convenience so we don't have to attach every token manually.
 */

let token = document.head.querySelector('meta[name="csrf-token"]');

if (token) {
    axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
} else {
    console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
}

axios.defaults.baseURL = baseUrl(true);
axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

if (typeof window.jQuery !== 'undefined') {
    axios.defaults.paramsSerializer = jQuery.param;
} else {
    axios.defaults.paramsSerializer = params => {
        return qs.stringify(params, {arrayFormat: 'brackets'});
    };
}

axios.interceptors.response.use(function (response) {
    handleResponse(response);
    return response;
}, function (error) {
    if (error.response) {
        handleResponse(error.response);
        if (error.response.status === 302) {
            return Promise.resolve(null);
        }
    }

    return Promise.reject(error);
});

function handleResponse(xhr)
{
    processEventsHeader(xhr);
    processMessageHeader(xhr);

    if (xhr.status > 200) {
        handleResponseStatus(xhr.status, xhr.responseJSON || (xhr.response ? xhr.response.data : xhr.data), xhr.statusText);
    }
}

function processEventsHeader(xhr) {
    let event = getXhrHeader(xhr, 'X-App-Event');

    if (event) {
        try {
            event = JSON.parse(event);
            if (event.event) {
                //TODO: EventBus.$emit(event.event, event.data);
            }
        } catch (err) {
            Notify.error(err.message);
        }
    }
}

function processMessageHeader(xhr) {
    let message = getXhrHeader(xhr, 'X-Flash-Message');

    if (message) {
        Notify.alert(decodeURIComponent(message), getXhrHeader(xhr, 'X-Flash-Message-Type'));
    }
}

function handleResponseStatus(status, response, statusText)
{
    switch(status) {
        case 302: {
            if (response && response.url) {
                const to = url(response.url, false);

                if (/https?:\/\//.test(to)) {
                    window.location.href = to;
                } else if (window.router) {
                    window.router.push(to);
                }
            }
            break;
        }

        case 422: {
            Notify.error(t('The form is invalid'));
            if (response && response.errors) {
                store.commit('form/setErrors', response.errors);
            }
            break;
        }

        case 401: window.location.reload(true);
            break;

        case 419:
            Notify.error('CSRF Token expired');
            window.location.reload(true);
            break;

        case 404:
            Notify.error('404. ' + t('Page not found'));
            break;

        case 200: break;

        default:
            if (status >= 400) {
                Notify.error(status + '. ' + t('An error occurred') + ': ' + (response.message || statusText));
            }
    }
}

function getXhrHeader(xhr, name) {
    if (typeof xhr.getResponseHeader === 'function') {
        return xhr.getResponseHeader(name);
    } else if(typeof xhr.headers === 'function') {
        return xhr.headers(name);
    } else if(typeof xhr.headers === 'object') {
        let n = name.toLowerCase();
        if (xhr.headers.hasOwnProperty(n)) {
            return xhr.headers[n];
        }
    }

    return undefined;
}

export default axios;
