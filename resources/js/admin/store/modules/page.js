const state = {
    loading: false
};

const mutations = {
    loading(state, loading) {
        state.loading = loading || false;
    }
};

export default {
    namespaced: true,
    state,
    mutations
}
