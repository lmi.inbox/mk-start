import {Storage} from "../../services/Storage";

const themeDefault = {
    accent: "#82B1FF",
    error: "#FF5252",
    info: "#2196F3",
    primary: "#1976D2",
    secondary: "#424242",
    success: "#4CAF50",
    warning: "#FFC107"
};

const themes = [
    {
        name: 'indigo',
        primary: '#3f51b5'
    },
    {
        name: 'deep-purple',
        primary: '#673AB7'
    },
    {
        name: 'blue',
        primary: '#1976D2'
    },
    {
        name: 'blue',
        primary: '#2196F3'
    },
    {
        name: 'light-blue',
        primary: '#03A9F4'
    },
    {
        name: 'teal',
        primary: '#009688'
    },
    {
        name: 'green',
        primary: '#4CAF50'
    },
    {
        name: 'light-green',
        primary: '#8BC34A'
    },
    {
        name: 'blue-grey',
        primary: '#607D8B'
    },
    {
        name: 'orange',
        primary: '#FF9800'
    },
    {
        name: 'amber',
        primary: '#FFC107'
    },
    {
        name: 'deep-orange',
        primary: '#FF5722'
    },
    {
        name: 'brown',
        primary: '#795548'
    },
    {
        name: 'red',
        primary: '#F44336',
    },
    {
        name: 'pink',
        primary: '#E91E63',
    },
    {
        name: 'purple',
        primary: '#9C27B0',
    },
    {
        name: 'grey',
        primary: '#9E9E9E'
    }
];

const state = {
    items: themes,
    current: null
};

const getters = {
    all: state => {
        return state.items;
    },

    current: state => {
        return state.current || Storage.getObject('admin-theme') || {name: 'indigo'};
    },

    light: (state, getters) => {
        return (getters.current.name || 'indigo') + ' lighten-5';
    },

    name: (state, getters) => {
        return getters.current.name;
    }
};

const actions = {
    set({dispatch, commit}, theme) {
        if (theme.vuetify) {
            commit('set', theme.theme);
            dispatch('apply', theme.vuetify);
        } else {
            commit('set', theme.theme);
        }
    },

    apply(context, $vuetify) {
        if (!$vuetify) {
            return;
        }

        const theme = context.getters.current || {};

        for (let i in themeDefault) {
            $vuetify.theme[i] = theme[i] || themeDefault[i];
        }
    },

    init({dispatch}, $vuetify) {
        dispatch('apply', $vuetify);
    }
};

const mutations = {
  set(state, theme) {
      state.current = theme;
      Storage.putObject('admin-theme', theme);
  }
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}
