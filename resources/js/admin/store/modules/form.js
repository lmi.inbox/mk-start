const state = {
    errors: {}
};

const getters = {
    errors: state => {
        return state.errors;
    },
};

const mutations = {
    clearErrors(state) {
        state.errors = {};
    },

    setErrors(state, errors) {
        state.errors = Object.assign({}, errors);
    },

    setFieldErrors(state, payload) {
        state.errors[payload.field] = payload.errors;
    }
};

export default {
    namespaced: true,
    state,
    mutations,
    getters
}
