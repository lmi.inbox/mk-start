import Vue from 'vue';
import Vuex from 'vuex';
import theme from './modules/theme';
import page from './modules/page';
import form from './modules/form';

Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        theme,
        page,
        form
    }
})
