import Config from './services/Config';

export function bootstrap(app, el = '#app') {
    const errorElem = document.getElementById("app-error");
    if (errorElem) {
        errorElem.remove();
    }

    const preload = document.getElementById("app-preload");
    if (preload) {
        preload.remove();
    }

    app.$mount(el);
}

export function bootstrapWithConfig(app, el = '#app') {
    Config.load().then(() => {
        bootstrap(app, el);
    }).catch(() => {
        let el = document.getElementById("app-error");
        if (el) {
            el.style.display = "block";
        }

        el = document.getElementById("app-preload");
        if (el) {
            el.remove();
        }
    });
}
