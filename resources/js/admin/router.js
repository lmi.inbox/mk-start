import Vue from 'vue';
import Router from 'vue-router';
import store from './store';

Vue.use(Router);

const router = new Router({
    routes: [
        {
            path: '/',
            redirect: '/dashboard',
        },
        /*{
            name: 'dashboard',
            path: '/dashboard',
            component: require('./pages/Dashboard'),
        },*/

        {
            path: '*',
            component: require('./pages/DynamicPage')
        }
    ],
});

router.beforeEach((to, from, next) => {
   store.commit('form/clearErrors');
   next();
});

export default router;
