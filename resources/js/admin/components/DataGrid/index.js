import Vue from 'vue';

import DataGrid from './DataGrid';
import CellHtml from './CellHtml';
import CellToggle from './CellToggle';
import CellBoolean from './CellBoolean';
import CellImage from './CellImage';

Vue.component('app-data-grid', DataGrid);
Vue.component('data-grid-cell', CellHtml);
Vue.component('data-grid-cell-toggle', CellToggle);
Vue.component('data-grid-cell-boolean', CellBoolean);
Vue.component('data-grid-cell-image', CellImage);
