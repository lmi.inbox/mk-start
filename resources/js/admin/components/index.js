import Vue from 'vue';

import Notifications from './Notifications';
import LanguagePicker from './LanguagePicker';
import ThemePicker from './ThemePicker';
import Navigation from './Navigation';
import Console from './Console';

import './DataGrid';

Vue.component('app-notifications', Notifications);
Vue.component('app-theme-picker', ThemePicker);
Vue.component('app-language-picker', LanguagePicker);
Vue.component('app-navigation', Navigation);
Vue.component('app-console', Console);
