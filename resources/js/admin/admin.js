import Vue from 'vue';
import router from './router';
import * as plugins from './plugins';
import './components';
import './directives';
import store from './store';
import {bootstrapWithConfig} from "./bootstrap";
import eventBus from './services/EventBus';
import Notify from "./services/Notify";

window.axios = plugins.axios;
window.router = router;

Vue.use(eventBus);

window.adminApp = new Vue({
    router,
    i18n: plugins.i18n,
    store,
    eventBus,

    data: {
        sidebar: null,
        username: null,
    },

    computed: {
        themeLight() {
            return this.$store.getters['theme/light'];
        }
    },

    created() {
        this.$store.dispatch('theme/init', this.$vuetify);
        Notify.init();
    },

    methods: {
        refreshPage: function() {
            //this.$eventBus.$emit('page-refresh');

            this.$router.replace({
                path: this.$router.currentRoute.fullPath,
                force: true,
                query: {
                    t: + new Date()
                }
            })
        },
    },
});

bootstrapWithConfig(window.adminApp);
