import Vue from 'vue';
import {url} from "./helpers";
import store from './store';

//logout button
Vue.directive('logout', {
    bind(el) {
        el.onclick = function(event) {
            event.preventDefault();
            const url = el.getAttribute('href') || 'logout';
            axios.post(url);
        }
    }
});

//Go back button (window.history.back())
Vue.directive('go-back', {
    bind(el) {
        el.onclick = function(event) {
            event.preventDefault();
            if (window.router) {
                window.router.go(-1);
            }
        }
    }
});

Vue.directive('admin-form', {
    bind(form, b, v) {
        form.onsubmit = function(e) {
            e.preventDefault();
            const formData = new FormData(form);

            const request = {
                method: form.method || 'POST',
                url: form.action,
                headers: {'X-Submit-Flag': form._submitFlag || 0}
            };

            if (request.method === 'GET' || request.method === 'get') {
                request.params = {};
                formData.forEach((v, k) => request.params[k] = v);
            } else {
                request.data = formData;
            }

            if (form._request) {
                form._request.cancel();
            }

            form._request = axios.CancelToken.source();
            request.cancelToken = form._request.token;

            store.commit('page/loading', true);
            store.commit('form/clearErrors');

            axios(request).then(response => {
                store.commit('page/loading', false);
                return response;
            }).catch(_ => {
                store.commit('page/loading', false);
            });
        }
    },

    unbind(form) {
        if (form._request) {
            form._request.cancel();
        }
    }
});

Vue.directive('admin-form-submit', {
    bind: function (el, bindings) {
        el.onclick = function(e) {
            //e.preventDefault();
            let flag = bindings.value || 0;

            const form = document.querySelector('form.admin-form');

            if (form) {
                form._submitFlag = flag;
            }

            return true;
        }
    }
});

Vue.directive('admin-form-cancel', {
    bind: function (btn) {
        btn.onclick = function(e) {
            e.preventDefault();
            if (window.router) {
                const path = btn.getAttribute('href');
                if (path) {
                    window.router.push(url(path));
                } else {
                    window.router.go(-1);
                }
            }
        }
    }
});
