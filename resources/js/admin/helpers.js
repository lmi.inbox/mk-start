function ltrimSlash(str) {
    if (str && str.charAt(0) === '/') {
        return str.substr(1);
    }

    return str;
}

function rtrimSlash(str) {
    if (str && str.charAt(str.length-1) === '/') {
        return str.slice(0, -1);
    }

    return str;
}

function baseUrl(absolute) {
    const base = (absolute ? window.baseURL : window.relativeURL) || '';

    return rtrimSlash(base) + '/';
}

function url(uri = null, absolute = false) {
    if (!uri || uri === '/') {
        return absolute ? baseUrl(true) : '/';
    }

    if (absolute && /https?:\/\//.test(uri)) {
        return uri;
    }

    let base = baseUrl(true);
    let relative = baseUrl(false);

    let path = uri;

    if (/https?:\/\//.test(path)) {
        path = path.substr(base.length);
    }

    if (!/https?:\/\//.test(path)) {
        path = '/' + ltrimSlash(path);

        if (path.startsWith(relative)) {
            path = path.substr(relative.length);
        }

        return (absolute ? base : '/') + ltrimSlash(path);
    }

    return path;
}

export {
    baseUrl,
    url
}
