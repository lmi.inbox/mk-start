@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="jumbotron-clear">
            <h1 class="display-3 text-center">
                Laravel
            </h1>

            <nav class="nav justify-content-center">
                <a class="nav-link" href="https://laravel.com/docs">Documentation</a>
                <a class="nav-link" href="https://laracasts.com">Laracasts</a>
                <a class="nav-link" href="https://laravel-news.com">News</a>
                <a class="nav-link" href="https://nova.laravel.com">Nova</a>
                <a class="nav-link" href="https://forge.laravel.com">Forge</a>
                <a class="nav-link" href="https://github.com/laravel/laravel">GitHub</a>
            </nav>

            @if (Route::has('login'))
                <div class="text-center mt-3">
                    @auth
                        <a class="btn btn-outline-primary" href="{{ url('/home') }}">Home</a>
                        <a class="btn btn-outline-primary" href="{{ route('logout') }}"
                           onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    @else
                        <a class="btn btn-outline-primary" href="{{ route('login') }}">Login</a>
                        <a class="btn btn-outline-primary" href="{{ route('register') }}">Register</a>
                    @endauth
                </div>
            @endif
        </div>
    </div>
@endsection
