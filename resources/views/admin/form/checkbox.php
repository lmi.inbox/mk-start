<?php if ($showLabel && $showField): ?>
    <?php if ($options['wrapper'] !== false): ?>
    <div <?= $options['wrapperAttrs'] ?> >
    <?php endif; ?>
<?php endif; ?>

<?php if ($showField): ?>
    <?php
        $label = null;

        if ($showLabel && $options['label'] !== false && $options['label_show']) {
            $label = $options['label'];
        }
    ?>

    <?= Form::checkbox($name, $options['value'], $options['checked'], $options['attr'], $label) ?>

    <?php include 'help_block.php' ?>
<?php endif; ?>

<?php include 'errors.php' ?>

<?php if ($showLabel && $showField): ?>
    <?php if ($options['wrapper'] !== false): ?>
    </div>
    <?php endif; ?>
<?php endif; ?>
