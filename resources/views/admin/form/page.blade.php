<v-card>
    {!! form_start($form) !!}

    @component('admin.header')

        {{$form->getTitle()}}

        @slot('tools')
            @foreach($form->getActions() as $action)
                @if ($form->has('action.' . $action))
                    {!! form_row($form->getField('action.' . $action)) !!}
                @endif
            @endforeach
        @endslot
    @endcomponent

    <v-card-text class="pa-4">
        {!! form_rest($form) !!}
    </v-card-text>

    {!! form_end($form) !!}

</v-card>
