<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }} Admin</title>

    <!-- Styles -->
    <link href="{{ asset('css/admin/admin.css') }}" rel="stylesheet">
</head>
<body>

<v-app id="app">
    <v-navigation-drawer
        fixed
        :clipped="$vuetify.breakpoint.lgAndUp"
        app
        v-model="sidebar"
        :class="themeLight"
        class="lighten-5"
    >
        <app-navigation></app-navigation>
    </v-navigation-drawer>

    <v-toolbar color="primary" dense dark app fixed :clipped-left="$vuetify.breakpoint.lgAndUp">
        <v-toolbar-side-icon @click.stop="sidebar = !sidebar" title="@lang('Menu')"></v-toolbar-side-icon>
        <v-toolbar-title>
            <span class="hidden-xs-only">@lang('Administration')</span>
        </v-toolbar-title>

        <v-spacer></v-spacer>

        <v-btn icon @click.stop="refreshPage" title="@lang('Reload')">
            <v-icon>refresh</v-icon>
        </v-btn>

        <v-btn icon href="{{url('/')}}" target="_blank" title="@lang('View site')">
            <v-icon>open_in_new</v-icon>
        </v-btn>

        <app-theme-picker title="@lang('Theme')"></app-theme-picker>
        <app-language-picker title="@lang('Language')"></app-language-picker>

        <v-menu offset-y bottom offset-x>
            <v-btn slot="activator" flat icon class="text-normal" title="@lang('Account')">
                <v-icon>person</v-icon>
            </v-btn>
            <v-list dense>
                <v-subheader class="justify-center">
                    @php
                    echo '{{ username ? username : "' . Auth()->user()->name . '" }}';
                    @endphp
                </v-subheader>
                <v-divider></v-divider>
                <v-list-tile href="{{route('logout')}}" v-logout>
                    <v-list-tile-title>@lang('Logout')</v-list-tile-title>
                </v-list-tile>
            </v-list>
        </v-menu>
    </v-toolbar>

    <v-content>
        <router-view></router-view>
        <app-notifications></app-notifications>
    </v-content>

</v-app>

<div id="app-preload" class="app-preload">
    <div class="v-progress-circular v-progress-circular--indeterminate primary--text loader-indicator">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="25 25 50 50" style="transform: rotate(0deg);">
            <circle fill="transparent" cx="50" cy="50" r="20" stroke-width="4" stroke-dasharray="125.664" stroke-dashoffset="125.66370614359172px" class="v-progress-circular__overlay"></circle>
        </svg>
    </div>
</div>

<div id="app-error" style="display: none">
    <div class="alert error" style="background-color: #FF5252">
        <i aria-hidden="true" class="material-icons icon alert__icon">warning</i>
        <div>
            Could not load configuration file. Please try again later.
        </div>
    </div>
</div>

<script>
    window.baseURL = "{{route('admin.home')}}/";
    window.relativeURL = "{{route('admin.home', [], false)}}";
</script>

<script src="{{ asset('js/vue-i18n-locales.generated.js') }}"></script>

<!--script src="{{ asset('js/admin/manifest.js') }}"></script>
<script src="{{ asset('js/admin/vendor.js') }}"></script-->
<script src="{{ asset('js/admin/admin.js') }}"></script>

</body>
</html>
