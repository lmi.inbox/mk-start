<v-list-tile @click="dataGrid.bulkDelete('{{ $url }}', '@lang('Are you sure?')')" class="error--text">
    <v-icon class="error--text">close</v-icon>
    <v-list-tile-title>{{ $slot->toHtml() ?: __('Delete') }}</v-list-tile-title>
</v-list-tile>