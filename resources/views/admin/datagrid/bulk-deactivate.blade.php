<v-list-tile @click="dataGrid.bulkUpdate('{{ $url }}')" class="warning--text">
    <v-icon class="warning--text">block</v-icon>
    <v-list-tile-title>{{ $slot->toHtml() ?: __('Deactivate') }}</v-list-tile-title>
</v-list-tile>
