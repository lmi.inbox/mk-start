@component('admin.datagrid.grid', [
    'title' => $title,
    'addUrl' => $addUrl,
    'search' => $searchable,
    'select' => $selectable
])

    @slot('menu')
        @foreach($menu as $action => $menuUrl)
            @if($menuUrl)
                @component('admin.datagrid.bulk-'.$action, ['url' => $menuUrl])@endcomponent
            @endif
        @endforeach
    @endslot

    <app-data-grid
            url="{{$url}}"
            {{$selectable ? 'selectable': ''}}
            {{$hasActions ? 'has-actions': ''}}
            item-key="{{$idKey}}"
            ref="dataGrid"
    >
        <template slot="actions" slot-scope="props">
            @foreach($actions as $action => $actionUrl)
                @if($actionUrl)
                    @component('admin.datagrid.action-'.$action, ['url' => $actionUrl])@endcomponent
                @endif
            @endforeach
        </template>
    </app-data-grid>

@endcomponent