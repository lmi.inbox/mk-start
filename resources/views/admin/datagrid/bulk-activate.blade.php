<v-list-tile @click="dataGrid.bulkUpdate('{{ $url }}')" class="success--text">
    <v-icon class="success--text">check</v-icon>
    <v-list-tile-title>{{ $slot->toHtml() ?: __('Activate') }}</v-list-tile-title>
</v-list-tile>