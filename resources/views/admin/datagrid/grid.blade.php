@component('admin.header')

    {{ $title ?? '' }}

    @slot('tools')

        @isset($addUrl)
            <v-btn
                    to="{{$addUrl}}"
                    title="@lang('Add')"
                    color="primary"
                    dark
                    small
                    absolute
                    bottom
                    left
                    fab
            >
                <v-icon>add</v-icon>
            </v-btn>
        @endisset

        @unless(isset($search) && !$search)
            <v-text-field
                          flat
                          append-icon="search"
                          label="@lang('Search')"
                          solo-inverted
                          v-model="dataGrid.search"
                          @keyup.enter="dataGrid.loadData"
                          clearable
                          class="input-field"
            ></v-text-field>
        @endunless

        @unless(isset($select) && !$select)
            <v-menu offset-y :disabled="!dataGrid.hasSelected">
                <v-btn icon slot="activator" dark :disabled="!dataGrid.hasSelected" title="@lang('With selected')">
                    <v-icon>more_vert</v-icon>
                </v-btn>
                <v-list dense>
                    {{ $menu ?? '' }}
                </v-list>
            </v-menu>
        @endunless

        {{ $tools ?? '' }}

    @endslot
@endcomponent

{{ $slot }}
