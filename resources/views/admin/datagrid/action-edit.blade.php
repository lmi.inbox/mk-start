<v-btn flat icon color="primary" :to="dataGrid.itemUrl('{{$url}}', props.item)" class="mx-0" title="@lang('Edit')">
    <v-icon small>edit</v-icon>
</v-btn>
