<v-btn icon flat color="error" @click="dataGrid.delete('{{$url}}', props.item, '@lang('Are you sure?')')"
       :disabled="props.item.__loading || dataGrid.itemExpression(props.item, 'disableDelete')" class="mx-0" title="@lang('Delete')">
    <v-icon small>close</v-icon>
</v-btn>
