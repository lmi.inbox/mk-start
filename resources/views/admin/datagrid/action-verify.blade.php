<v-btn icon flat color="success" @click="dataGrid.action('{{$url}}', props.item, '@lang('Are you sure?')')"
       :disabled="props.item.__loading || dataGrid.itemExpression(props.item, 'disableVerify')" class="mx-0" title="@lang('Verify')">
    <v-icon small>check</v-icon>
</v-btn>
