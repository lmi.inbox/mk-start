<v-toolbar color="primary" dark class="page-toolbar">
    {{$icon ?? ''}}
    <v-toolbar-title class="white--text page-title">{{ $slot }}</v-toolbar-title>
    <v-spacer></v-spacer>
    {{ $tools ?? '' }}
</v-toolbar>