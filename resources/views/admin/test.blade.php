<v-toolbar class="page-toolbar">
    <v-breadcrumbs>
        <v-icon slot="divider">chevron_right</v-icon>

        <v-breadcrumbs-item>
            Home
        </v-breadcrumbs-item>
        <v-breadcrumbs-item>
            Users
        </v-breadcrumbs-item>
    </v-breadcrumbs>
</v-toolbar>