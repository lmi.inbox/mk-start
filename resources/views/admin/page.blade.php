<v-card>

    @component('admin.header')
        {{ $title ?? '' }}
        @slot('tools')
            {{ $tools ?? '' }}
        @endslot
    @endcomponent

    <v-card-text>
        {{ $slot }}
    </v-card-text>

</v-card>