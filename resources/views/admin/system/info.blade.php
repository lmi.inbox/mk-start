<v-toolbar color="primary" dark tabs class="page-toolbar">
    <v-icon class="toolbar-side-icon">info</v-icon>
    <v-toolbar-title class="page-title">@lang('System Information')</v-toolbar-title>
    <v-spacer></v-spacer>

    <v-tabs
            slot="extension"
            color="primary"
            align-with-title
            v-model="model.info"
    >
        <v-tab href="#info-system">
            @lang('System Information')
        </v-tab>

        <v-tab href="#info-apache" v-if="model.apache">
            @lang('Apache')
        </v-tab>

        <v-tab href="#info-mysql" v-if="model.mysql">
            Mysql
        </v-tab>

        <v-tab href="#info-php">
            PHP
        </v-tab>

        <v-tab href="#info-vars">
            @lang('Server Variables')
        </v-tab>

        <v-tabs-slider></v-tabs-slider>
    </v-tabs>

</v-toolbar>

<v-tabs-items v-model="model.info">

    <v-tab-item id="info-system">
        <v-card flat v-if="model.system">
            <v-card-text>
                <table class="v-table table-striped theme--light">
                    <tbody>
                    <tr v-for="(v, k) in model.system">
                        <td class="body-1 grey--text text--darken-1">@{{$t('messages.info.' + k)}}</td>
                        <td>@{{v}}</td>
                    </tr>
                    </tbody>
                </table>
            </v-card-text>
        </v-card>
    </v-tab-item>

    <v-tab-item id="info-apache">
        <v-card flat v-if="model.apache">
            <v-card-title>
                <div class="subheading grey--text text--darken-1">@{{model.apache.version}}</div>
            </v-card-title>
            <v-card-text>
                <div class="body-2 grey--text text--darken-1">@lang('Modules') (@{{ model.apache.modules.length }})</div>
                <br />
                <v-chip v-for="v in model.apache.modules" :key="v" label>@{{v}}</v-chip>
            </v-card-text>
        </v-card>
    </v-tab-item>

    <v-tab-item id="info-mysql">
        <v-card flat v-if="model.mysql">
            <v-card-text>
                <table class="v-table theme--light">
                    <tbody>
                    <tr v-for="(v, k) in model.mysql">
                        <td class="body-1 grey--text text--darken-1">@{{k}}</td>
                        <td>@{{v}}</td>
                    </tr>
                    </tbody>
                </table>
            </v-card-text>
        </v-card>
    </v-tab-item>

    <v-tab-item id="info-php">
        <v-card flat v-if="model.php">
            <v-card-text>
                <v-layout row>
                    <v-flex>
                        <div class="title grey--text text--darken-1">PHP @{{model.php.version}}</div>
                        <small class="grey--text">
                            @lang('Built on'): @{{model.php.built_on}}
                        </small>
                    </v-flex>
                    <v-btn v-if="model.php.info_url" :href="model.php.info_url" color="primary" dark target="_blank">@lang('Full info')</v-btn>
                </v-layout>
                <v-expansion-panel focusable popout :value="0">
                    <v-expansion-panel-content>
                        <div slot="header">@lang('Settings')</div>
                        <v-card>
                            <v-card-text>
                                <table class="v-table theme--light">
                                    <thead>
                                    <tr>
                                        <th width="250">@lang('Setting')</th>
                                        <th>@lang('Value')</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr v-for="(v, k) in model.php.settings">
                                        <td class="body-1 grey--text text--darken-1">@{{k}}</td>
                                        <td>@{{v}}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </v-card-text>
                        </v-card>
                    </v-expansion-panel-content>
                    <v-expansion-panel-content>
                        <div slot="header">@lang('Extensions')</div>
                        <v-card>
                            <v-card-text>
                                <v-chip v-for="v in model.php.extensions" :key="v" label>@{{v}}</v-chip>
                            </v-card-text>
                        </v-card>
                    </v-expansion-panel-content>
                </v-expansion-panel>
            </v-card-text>
        </v-card>
    </v-tab-item>

    <v-tab-item id="info-vars">
        <v-card flat v-if="model.vars">
            <v-card-text>
                <v-expansion-panel focusable popout :value="0">
                    <v-expansion-panel-content>
                        <div slot="header">@lang('Server Variables')</div>
                        <table class="v-table theme--light">
                            <tbody>
                            <tr v-for="(v,k) in model.vars.server">
                                <td class="body-1 grey--text text--darken-1">@{{k}}</td>
                                <td style="word-break: break-all">@{{v}}</td>
                            </tr>
                            </tbody>
                        </table>
                    </v-expansion-panel-content>

                    <v-expansion-panel-content>
                        <div slot="header">@lang('Environment Variables')</div>
                        <table class="v-table theme--light">
                            <tbody>
                            <tr v-for="(v,k) in model.vars.env">
                                <td class="body-1 grey--text text--darken-1">@{{k}}</td>
                                <td>@{{v}}</td>
                            </tr>
                            </tbody>
                        </table>
                    </v-expansion-panel-content>

                </v-expansion-panel>
            </v-card-text>
        </v-card>
    </v-tab-item>

</v-tabs-items>
