@component('admin.page')
    @slot('title')
        @lang('Dashboard')
    @endslot

    Dashboard content
@endcomponent
