@extends('profile.index')

@section('header')
    <nav class="nav nav-tabs card-header-tabs">
        <a class="nav-link" href="{{route('profile')}}">Profile</a>
        <a class="nav-link active">Password</a>
    </nav>
@endsection

@section('form')
    <form method="POST" action="{{ route('profile.password.update') }}">
        @csrf

        <div class="form-group row">
            <label for="current_password" class="col-md-4 col-form-label text-md-right">
                {{ __('Current Password') }}
            </label>

            <div class="col-md-6">
                <input id="current_password" type="password"
                       class="form-control{{ $errors->has('current_password') ? ' is-invalid' : '' }}"
                       name="current_password" required autofocus>

                @if ($errors->has('current_password'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('current_password') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group row">
            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('New Password') }}</label>

            <div class="col-md-6">
                <input id="password" type="password"
                       class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                @if ($errors->has('password'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group row">
            <label for="password-confirm"
                   class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

            <div class="col-md-6">
                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
            </div>
        </div>

        <div class="form-group row mb-0">
            <div class="col-md-6 offset-md-4">
                <button type="submit" class="btn btn-primary">{{ __('Save') }}</button>
            </div>
        </div>
    </form>
@endsection
