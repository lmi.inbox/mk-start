<?php
/**
 * User: mike
 * Date: 23.11.17
 * Time: 17:58
 */

return [
    'mail' => [
        'subcopy' => 'If you’re having trouble clicking the ":actionText" button, copy and paste the URL below into your web browser: [:actionUrl](:actionUrl)'
    ],

    'info' => [
        'webserver' => 'Web Server',
        'apache_version' => 'Apache Version',
        'apache_version_number' => 'Apache Version Number',
        'php_version' => 'PHP Version',
        'php_built_on' => 'PHP Built On',
        'sapi_name' => 'WebServer to PHP Interface',
        'user_agent' => 'User Agent',
        'db_server' => 'Database Server',
        'db_version' => 'Database Version',
        'db_collation' => 'Database Collation',
        'db_connection_collation' => 'Database Connection Collation'
    ]
];