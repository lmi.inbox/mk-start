<?php
/**
 * User: mike
 * Date: 23.11.17
 * Time: 17:58
 */

return [
    'mail' => [
        'subcopy' => 'Якщо у вас виникають проблеми при натисканні кнопки ":actionText", скопіюйте та вставте URL нижче в свій веб-браузер: [:actionUrl](:actionUrl)'
    ],

    'info' => [
        'webserver' => 'Веб-сервер',
        'apache_version' => 'Версія Apache',
        'php_version' => 'Версія PHP',
        'php_built_on' => 'Базується на PHP',
        'sapi_name' => 'Веб-сервер з інтерфейсом PHP',
        'user_agent' => 'Агент користувача (User Agent)',
        'db_server' => 'Сервер бази даних',
        'db_version' => 'Версія бази даних',
        'db_collation' => 'Співставлення бази даних',
        'db_connection_collation' => 'Підключення співставлення бази даних'
    ]
];