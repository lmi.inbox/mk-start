<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => 'Ви повинні прийняти :attribute.',
    'active_url'           => 'Поле :attribute містить неправильне посилання.',
    'after'                => 'Поле :attribute повинно бути більшим за :date.',
    'after_or_equal'       => 'Поле :attribute повинно бути більшим або рівним :date.',
    'alpha'                => 'Поле :attribute може містити тільки літери.',
    'alpha_dash'           => 'Поле :attribute може містити тільки літери, цифри, тире і підкреслення.',
    'alpha_num'            => 'Поле :attribute може містити тільки літери та цифри.',
    'array'                => 'Поле :attribute повинно бути масивом.',
    'before'               => 'Поле :attribute повинно бути меншим за :date.',
    'before_or_equal'      => 'Поле :attribute повинно бути меншим або рівним :date.',
    'between'              => [
        'numeric' => 'Поле :attribute повинно бути між :min та :max.',
        'file'    => 'Розмір файлу :attribute повинно бути не менше :min та не більше :max кілобайт.',
        'string'  => 'Текст :attribute повинен бути не менше :min та не більше :max символів.',
        'array'   => 'Поле :attribute повинно містити від :min до :max елементів.',
    ],
    'boolean'              => 'Поле :attribute повинно бути логічного типу.',
    'confirmed'            => 'Підтвердження для поля :attribute не співпадає.',
    'date'                 => 'Поле :attribute не є датою.',
    'date_format'          => 'Поле :attribute не відповідає формату :format.',
    'different'            => 'Поля :attribute та :other повинні бути різними.',
    'digits'               => 'Довжина цифрового поля :attribute повинна дорівнювати :digits.',
    'digits_between'       => 'Довжина цифрового поля :attribute повинна бути від :min до :max.',
    'dimensions'           => 'Поле :attribute містіть неприпустимі розміри зображення.',
    'distinct'             => 'Поле :attribute містить значення, яке дублюється.',
    'email'                => 'Поле :attribute повинне містити електронну адресу.',
    'exists'               => 'Вибране для :attribute значення не коректне.',
    'file'                 => 'Поле :attribute пофинно містити файл.',
    'filled'               => "Поле :attribute є обов'язковим для заповнення.",
    'image'                => 'Поле :attribute має містити зображення.',
    'in'                   => 'Вибране для :attribute значення не коректне.',
    'in_array'             => 'Значення поля :attribute не міститься в :other.',
    'integer'              => 'Поле :attribute повинно містити ціле число.',
    'ip'                   => 'Поле :attribute повинно містити IP адресу.',
    'ipv4'                 => 'Поле :attribute повинно містити IPv4 адресу.',
    'ipv6'                 => 'Поле :attribute повинно містити IPv6 адресу.',
    'json'                 => 'Поле :attribute повинно бути у форматі JSON.',
    'max'                  => [
        'numeric' => 'Поле :attribute повинно бути не більше :max.',
        'file'    => 'Файл :attribute повинен бути не більше :max кілобайт.',
        'string'  => 'Текст :attribute повинен мати довжину не більшу за :max.',
        'array'   => 'Поле :attribute повинне містити не більше :max елементів.',
    ],
    'mimes'                => 'Поле :attribute повинно містити файл одного з типів: :values.',
    'mimetypes'            => 'Поле :attribute повинно бути файлом з типів: :values.',
    'min'                  => [
        'numeric' => 'Поле :attribute повинно бути не менше :min.',
        'file'    => 'Файл :attribute повинен бути не меншим :min кілобайт.',
        'string'  => 'Текст :attribute повинен містити не менше :min символів.',
        'array'   => 'Поле :attribute повинне містити не менше :min елементів.',
    ],
    'not_in'               => 'Вибране для :attribute значення не коректне.',
    'numeric'              => 'Поле :attribute повинно містити число.',
    'present'              => 'Поле :attribute повинно бути присутнім.',
    'regex'                => 'Поле :attribute має невірний формат.',
    'required'             => "Поле :attribute є обов'язковим для заповнення.",
    'required_if'          => "Поле :attribute є обов'язковим для заповнення, коли :other є рівним :value.",
    'required_unless'      => "Поле :attribute є обов'язковим для заповнення, коли :other відрізняється від :values",
    'required_with'        => "Поле :attribute є обов'язковим для заповнення, коли :values вказано.",
    'required_with_all'    => "Поле :attribute є обов'язковим для заповнення, коли :values вказано.",
    'required_without'     => "Поле :attribute є обов'язковим для заповнення, коли :values не вказано.",
    'required_without_all' => "Поле :attribute є обов'язковим для заповнення, коли :values не вказано.",
    'same'                 => 'Поля :attribute та :other повинні співпадати.',
    'size'                 => [
        'numeric' => 'Поле :attribute повинно бути довжиною :size.',
        'file'    => 'Файл :attribute повинно бути розміром :size кілобайт.',
        'string'  => 'Текст :attribute повинен містити :size символів.',
        'array'   => 'Поле :attribute повинно містити :size елементів.',
    ],
    'string'               => 'Поле :attribute повинно містити текст.',
    'timezone'             => 'Поле :attribute повинно містити коректну часову зону.',
    'unique'               => 'Таке значення поля :attribute вже існує.',
    'uploaded'             => 'Неможливо завантажити файл :attribute.',
    'url'                  => 'Формат поля :attribute неправильний.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [
        'name' => 'Ім\'я',
        'email' => 'E-mail',
        'password' => 'Пароль',
        'password_current' => 'Поточний пароль',
        'password_new' => 'Новий пароль',
    ],

];
