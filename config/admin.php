<?php

return [

    'url' => env('ADMIN_URL', 'admin'),
    'locale' => env('ADMIN_LOCALE', 'en'),
    'locales' => ['en', 'uk'],

    'menu' => [
        ['title' => 'Dashboard', 'href' => '#/dashboard', 'icon' => 'dashboard'],
        ['title' => 'Users', 'href' => '#/user', 'icon' => 'person'],
        [
            'title' => 'System', 'icon' => 'settings',
            'children' => [
                ['title' => 'System Information', 'href' => '#/info'],
                ['title' => 'Console', 'href' => '#/console'],
            ]
        ]
    ],

    'console' => [
        'only' => [], //show only these console commands
        'exclude' => [
            'app:name',
            'preset',
            'serve',
            'ide-helper:models',
            'ide-helper:eloquent',
            'migrate:fresh',
            'migrate:install',
            'migrate:refresh',
            'migrate:reset',
            'db:seed',
            'make:auth',
            'dump-server'
        ] //exclude these commands
    ]
];
