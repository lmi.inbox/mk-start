<?php

return [
    'defaults'      => [
        'wrapper_class'       => 'form-group',
        'wrapper_error_class' => 'has-error',
        'label_class'         => 'control-label',
        'field_class'         => 'form-control',
        'help_block_class'    => 'help-block',
        'error_class'         => 'text-danger',
        'required_class'      => 'required'

        // Override a class from a field.
        //'text'                => [
        //    'wrapper_class'   => 'form-field-text',
        //    'label_class'     => 'form-field-text-label',
        //    'field_class'     => 'form-field-text-field',
        //]
        //'radio'               => [
        //    'choice_options'  => [
        //        'wrapper'     => ['class' => 'form-radio'],
        //        'label'       => ['class' => 'form-radio-label'],
        //        'field'       => ['class' => 'form-radio-field'],
        //],
    ],
    // Templates
    'form'          => 'form',
    'text'          => 'text',
    'textarea'      => 'textarea',
    'button'        => 'button',
    'buttongroup'   => 'buttongroup',
    'radio'         => 'radio',
    'checkbox'      => 'checkbox',
    'select'        => 'select',
    'choice'        => 'choice',
    'repeated'      => 'repeated',
    'child_form'    => 'child_form',
    'collection'    => 'collection',
    'static'        => 'static',

    // Remove the laravel-form-builder:: prefix above when using template_prefix
    'template_prefix'   => 'laravel-form-builder::',

    'default_namespace' => '',

    'custom_fields' => [
//        'datetime' => App\Forms\Fields\Datetime::class,
        'admin.action' => \App\Forms\Admin\FormAction::class
    ]
];
