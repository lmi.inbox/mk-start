<?php
/**
 * User: mike
 * Date: 5/14/2018
 * Time: 12:46
 */

namespace App\Traits;


use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

trait HasModel
{
    public function modelClass()
    {
        if (!property_exists($this, 'modelClass')) {
            abort(500, 'Property modelClass does not exists');
        }

        return $this->modelClass;
    }

    /**
     * @param Model|mixed $model
     * @return Model|null
     */
    public function model($model = null)
    {
        if (!$model) {
            return new $this->modelClass();
        }

        if (!$model instanceof Model) {
            $model = call_user_func([$this->modelClass(), 'find'], $model);
        }

        return $model;
    }

    /**
     * @return Builder
     */
    public function modelQuery(): Builder
    {
        return $this->modelClass()::query();
    }
}