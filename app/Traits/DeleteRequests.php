<?php
/**
 * User: mike
 * Date: 5/14/2018
 * Time: 13:41
 */

namespace App\Traits;


use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

trait DeleteRequests
{
    /**
     * @return Builder
     */
    protected function deletable(): Builder
    {
        return $this->modelQuery();
    }

    /**
     * @param Request $request
     * @param null $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(Request $request, $id = null)
    {
        if ($id === null) {
            $id = $request->get('id', []);
        }

        $model = $this->model();

        $deleteMethod = 'delete';

        if (in_array(SoftDeletes::class, class_uses_recursive($model))) {
            $deleteMethod = 'forceDelete';
        }

        $result = $this->deletable()->whereIn($model->getKeyName(), (array) $id)->$deleteMethod();

        if (!$result) {
            app()->abort(422);
        }

        return response()->json($id);
    }
}