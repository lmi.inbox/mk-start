<?php
/**
 * User: mike
 * Date: 5/14/2018
 * Time: 12:44
 */

namespace App\Traits;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;


/**
 * Trait ToggleRequests
 * @package App\Traits
 *
 * @property string $toggleField
 */
trait ToggleRequests
{
    /**
     * @param Request $request
     * @param null $model
     * @return \Illuminate\Http\JsonResponse
     */
    public function toggle(Request $request, $model = null)
    {
        $model = $this->toggleable($model)->find($model ?: $request->get('id'));

        $model->toggle();
        $model->save();

        return response()->json([
            $model->activeField() => $model->isActive()
        ]);
    }

    /**
     * @param Request $request
     * @param $status
     * @return \Illuminate\Http\JsonResponse
     */
    public function bulkToggle(Request $request, $status)
    {
        $id = (array) $request->get('id', []);
        $model = $this->model();

        $field = $model->activeField();
        $idField = $model->getKeyName();

        $result = $this->toggleable()->whereIn($idField, $id)->update([
            $field => $status
        ]);

        if (!$result) {
            app()->abort(402);
        }

        $data = [];
        $models = $model::whereIn($idField, $id)->get();

        foreach($models as $model) {
            $data[] = [
                'id' => $model->getKey(),
                $field => $model->$field
            ];
        }

        return response()->json($data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function activate(Request $request)
    {
        return $this->bulkToggle($request, true);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deactivate(Request $request)
    {
        return $this->bulkToggle($request, false);
    }

    /**
     * @param Model|null $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function toggleable($model = null): Builder
    {
        return $this->model($model)->newQuery();
    }
}