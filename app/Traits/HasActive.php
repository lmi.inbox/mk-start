<?php
/**
 * Author: mike
 * Date: 24.03.17
 * Time: 13:22
 */

namespace App\Traits;
use Illuminate\Database\Eloquent\Builder;


/**
 * Class HasActive
 * @package App\Traits
 *
 * @property bool $active
 * @method static Builder active()
 */
trait HasActive
{
    /**
     * @return string
     */
    public function activeField()
    {
        return 'active';
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    public function scopeActive($query)
    {
        return $query->where($this->activeField(), true);
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    public function scopeInactive($query)
    {
        return $query->where($this->activeField(), false);
    }

    /**
     * @param $value
     * @return bool
     */
    public function getActiveAttribute($value = null)
    {
        return boolval(is_null($value) ? $this->getAttribute($this->activeField()) : $value);
    }

    /**
     * @param $value
     * @return bool
     */
    public function setActiveAttribute($value)
    {
        return $this->attributes[$this->activeField()] = boolval($value);
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->getAttribute($this->activeField());
    }

    /**
     * @return $this
     */
    public function toggle()
    {
        $this->setActiveAttribute(!$this->getAttribute($this->activeField()));
        return $this;
    }
}