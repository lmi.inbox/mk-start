<?php
/**
 * User: mike
 * Date: 09.05.18
 * Time: 12:57
 */

namespace App\Services;


use Illuminate\Database\DatabaseManager;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class SysInfo
{
    /**
     * @var Collection
     */
    private $info;

    /**
     * @var Request
     */
    private $request;

    /**
     * @var DatabaseManager
     */
    private $dbManager;

    /**
     * Server constructor.
     * @param Request $request
     * @param DatabaseManager $db
     */
    public function __construct(Request $request, DatabaseManager $db)
    {
        $this->request = $request;
        $this->dbManager = $db;
    }

    /**
     * @param null $key
     * @param null $default
     * @return Collection|mixed
     */
    public function getSystemInfo($key = null, $default = null)
    {
        if (!isset($this->info)) {
            $info = [];

            $info['webserver'] = $this->request->server->get('SERVER_SOFTWARE', env('SERVER_SOFTWARE'));

            $info['php_version'] = phpversion();
            $info['php_built_on'] = php_uname();
            $info['sapi_name'] = php_sapi_name();

            $info['db_server'] = $this->getDbServer();
            $info['db_version'] = $this->getDbVersion();

            if ($collation = $this->getDbCollation()) {
                $info['db_collation'] = $collation;
                $info['db_connection_collation'] = $this->getDbConnectionCollation();
            }

            $info['user_agent'] = $this->request->server->get('HTTP_USER_AGENT');

            $this->getDbCollation();

            $this->info = new Collection($info);
        }

        return $key ? $this->info->get($key, $default) : $this->info;
    }

    /**
     * @return bool
     */
    public function isApache(): bool
    {
        return function_exists('apache_get_version');
    }

    /**
     * @return false|string
     */
    public function getApacheVersion()
    {
        return apache_get_version();
    }

    /**
     * @return array
     */
    public function getApacheModules(): array
    {
        $result = [];

        if (function_exists('apache_get_modules')) {
            $apacheModules = apache_get_modules();
            sort($apacheModules);

            $result = $apacheModules;
        }

        return $result;
    }

    /**
     * @return bool
     */
    public function isMysql(): bool
    {
        return strtolower($this->getDbServer()) === 'mysql';
    }

    /**
     * @return array|false
     */
    public function getMysqlInfo()
    {
        if (!$this->isMysql()) {
            return false;
        }

        $result = [];

        $db = $this->dbManager->connection();

        if ($db) {
            $attributes = [
                'SERVER_VERSION',
                'SERVER_INFO',
                'CLIENT_VERSION',
                'AUTOCOMMIT',
                'CASE',
                'CONNECTION_STATUS',
                'DRIVER_NAME',
                'ERRMODE',
                'ORACLE_NULLS',
                'PERSISTENT',
                'PREFETCH',
                'TIMEOUT'
            ];

            $pdo = $db->getPdo();

            foreach ($attributes as $val) {
                try {
                    $result[$val] = $pdo->getAttribute(constant("PDO::ATTR_$val"));
                } catch (\Exception $e) {}
            }
        }

        return $result;
    }

    /**
     * @return array
     */
    public function getPhpSettings(): array
    {
        return [
            'Safe Mode' => self::bool2str(ini_get('safe_mode')),
            'Open basedir' => ini_get('open_basedir'),
            'Display Errors' => self::bool2str(ini_get('display_errors')),
            'Error Reporting' => ini_get('error_reporting'),
            'Short Open Tags' => self::bool2str(ini_get('short_open_tag')),
            'File Uploads' => self::bool2str(ini_get('file_uploads')),
            'Magic Quotes' => self::bool2str(ini_get('magic_quotes_gpc')),
            'Register Globals' => self::bool2str(ini_get('register_globals')),
            'Output Buffering' => self::bool2str(ini_get('output_buffering')),
            'Session Save Path' => ini_get('session.save_path'),
            'Session Auto Start' => ini_get('session.auto_start'),
            'XML Enabled' => self::bool2str(extension_loaded('xml'), true),
            'Zlib Enabled' => self::bool2str(extension_loaded('zlib'), true),
            'Native ZIP Enabled' => self::bool2str(function_exists('zip_open') && function_exists('zip_read'), true),
            'Disabled Functions' => str_replace(',', ' ', ini_get('disable_functions')),
            'Mbstring Enabled' => self::bool2str(extension_loaded('mbstring'), true),
            'Iconv Available' => self::bool2str(extension_loaded('iconv'), true),
        ];
    }

    /**
     * @param $val
     * @param bool $yesno
     * @return mixed|string
     */
    public static function bool2str($val, $yesno = false)
    {
        $lc = strtolower($val);

        if (in_array($lc, ['on', 'off', 'yes', 'no', 'y', 'n'])) {
            return $val;
        }

        $result = (bool) $val ? 'On' : 'Off';

        if ($yesno) {
            $result = str_replace(['On', 'Off'], ['Yes', 'No'], $result);
        }

        return $result;
    }

    /**
     * @return string
     */
    public function getPhpVersion()
    {
        return $this->getSystemInfo('php_version');
    }

    /**
     * @return string
     */
    public function getPhpBuiltOn()
    {
        return $this->getSystemInfo('php_built_on');
    }

    /**
     * @return string
     */
    public function getPhpSapi()
    {
        return $this->getSystemInfo('sapi_name');
    }

    /**
     * @return string
     */
    public function getUserAgent()
    {
        return $this->getSystemInfo('user_agent');
    }

    /**
     * @return array
     */
    public function getPhpExtensions(): array
    {
        $result = get_loaded_extensions();
        sort($result);

        return $result;
    }

    /**
     * @return array
     */
    public function getServerVariables(): array
    {
        $result = $this->request->server->all();
        ksort($result);

        return $result;
    }

    /**
     * @return mixed
     */
    public function getEnv()
    {
        $result = $_ENV;
        ksort($result);

        return $result;
    }

    /**
     * @return string
     */
    public function getDbServer()
    {
        return $this->dbManager->getDriverName();
    }

    /**
     * @return string
     */
    public function getDbVersion()
    {
        return $this->dbManager->getPdo()->getAttribute(\PDO::ATTR_SERVER_VERSION);
    }

    /**
     * @return string|bool
     */
    public function getDbCollation()
    {
        if (!$this->isMysql()) {
            return false;
        }

        $stmt = $this->dbManager->getPdo()->query('SHOW VARIABLES LIKE "collation_database"');

        if ($stmt) {
            if ($row = $stmt->fetch()) {
                return $row['Value'] ?? false;
            }
        }

        return false;
    }

    /**
     * @return string|bool
     */
    public function getDbConnectionCollation()
    {
        if (!$this->isMysql()) {
            return false;
        }

        $stmt = $this->dbManager->getPdo()->query('SHOW VARIABLES LIKE "collation_connection"');

        if ($stmt) {
            if ($row = $stmt->fetch()) {
                return $row['Value'] ?? false;
            }
        }

        return false;
    }
}