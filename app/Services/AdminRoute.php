<?php
/**
 * User: mike
 * Date: 5/14/2018
 * Time: 10:41
 */

namespace App\Services;


use Illuminate\Routing\Router;

class AdminRoute
{
    /**
     * @return Router
     */
    protected static function router()
    {
        return app('router');
    }

    /**
     * @param string $controller
     * @param Router|null $router
     * @param bool $toggle
     * @param bool $move
     * @param bool $trash
     * @param \Closure|null $routes
     */
    public static function crud(string $controller, Router $router = null, $toggle = false, $move = false, $trash = false, $routes = null)
    {
        if (!$router) {
            $router = self::router();
        }

        $router->get('/', $controller . '@index')->name('index');
        $router->get('data', $controller . '@data')->name('data');

        $router->get('show/{model}', $controller . '@show')->name('show');
        $router->get('create', $controller . '@create')->name('create');
        $router->post('store', $controller . '@store')->name('store');
        $router->get('edit/{model}', $controller . '@edit')->name('edit');
        $router->post('update/{model}', $controller . '@update')->name('update');
        $router->post('delete/{id?}', $controller . '@delete')->name('delete');

        if ($toggle) {
            $router->post('toggle/{model?}', $controller . '@toggle')->name('toggle');
            $router->post('activate', $controller . '@activate')->name('activate');
            $router->post('deactivate', $controller . '@deactivate')->name('deactivate');
        }

        if ($move) {
            $router->post('move/{model?}/{down?}', $controller . '@move')->name('move');
        }

        if ($trash) {
            $router->post('trash/{model?}', $controller . '@toTrash')->name('toTrash');
            $router->post('restore/{model?}', $controller . '@restore')->name('restore');
        }

        if ($routes instanceof \Closure) {
            $routes($router);
        }
    }

    /**
     * @param string $controller
     * @param string $prefix
     * @param string|null $as
     * @param array $attributes
     * @param \Closure|null $routes
     */
    public static function group(string $controller, string $prefix, string $as = null, array $attributes = [], $routes = null)
    {
        $router = self::router();

        $toggle = array_pull($attributes, 'toggle', false);
        $move = array_pull($attributes, 'move', false);
        $trash = array_pull($attributes, 'trash', false);

        $attr = array_merge([
            'prefix' => $prefix,
            'as' => is_null($as) ? $prefix . '.' : $as
        ], $attributes);

        $router->group($attr, function($router) use ($controller, $toggle, $move, $trash, $routes) {
            static::crud($controller, $router, $toggle, $move, $trash, $routes);
        });
    }
}
