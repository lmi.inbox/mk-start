<?php
/**
 * User: mike
 * Date: 10/19/2018
 * Time: 11:36
 */

namespace App\Services;


class HtmlBuilder extends \Collective\Html\HtmlBuilder
{
    /**
     * @param string $key
     * @param string $value
     * @return string
     */
    function attributeElement($key, $value)
    {
        // For numeric keys we will assume that the value is a boolean attribute
        // where the presence of the attribute represents a true value and the
        // absence represents a false value.
        // This will convert HTML attributes such as "required" to a correct
        // form instead of using incorrect numerics.
        if (is_numeric($key)) {
            return $value;
        }

        // Treat boolean attributes as HTML properties
        if (is_bool($value) && $key !== 'value') {
            return $value ? $key : '';
        }

        if (is_array($value) && $key === 'class') {
            return 'class="' . implode(' ', $value) . '"';
        }

        if (! is_null($value)) {
            if (starts_with($key, ':') || $key === 'label') {
                return sprintf('%s="%s"', $key, $value);
            }

            return $key . '="' . e($value, false) . '"';
        }
    }
}
