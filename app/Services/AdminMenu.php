<?php

namespace App\Services;


use App\Contracts\IAdminMenuBuilder;
use Lavary\Menu\Builder as MenuBuilder;
use Lavary\Menu\Facade as Menu;
use Lavary\Menu\Item;
use Illuminate\Contracts\Auth\Access\Gate;

class AdminMenu
{
    /**
     * @var array
     */
    private $configItems = [];

    /**
     * @var IAdminMenuBuilder[];
     */
    private $builders = [];

    /**
     * AdminMenu constructor.
     * @param array $items
     * @param array $builders
     */
    public function __construct(array $items = [], array $builders = [])
    {
        $this->configItems = $items;

        foreach ($builders as $builder) {
            $this->addBuilder($builder);
        }
    }

    /**
     * @param IAdminMenuBuilder $builder
     */
    public function addBuilder(IAdminMenuBuilder $builder)
    {
        $this->builders[] = $builder;
    }

    /**
     * @return array
     */
    public function items(): array
    {
        $items = $this->configItems;

        /** @var MenuBuilder $menu */
        $menu = Menu::make('adminMenu', function($menu) use ($items) {
            /** @var MenuBuilder $menu */
            $this->buildFromArray($menu, $items);
        });

        foreach ($this->builders as $builder) {
            $builder->build($menu);
        }

        return $this->menuToArray($menu);
    }

    /**
     * @param MenuBuilder $menu
     * @param null $parent
     * @return array
     */
    private function menuToArray(MenuBuilder $menu, $parent = null): array
    {
        $items = [];

        /** @var Item $item */
        foreach ($menu->whereParent($parent) as $item)
        {
            $data = $item->attributes;

            if (isset($data['href'])) {
                $data['url'] = $data['href'];
                unset($data['href']);
            } else {
                $data['url'] = $item->url();
            }

            $data['title'] = $item->title;

            if( $item->hasChildren() ) {
                $data['children'] = $this->menuToArray($menu, $item->id);
            }

            $items[] = $data;
        }

        return $items;
    }

    /**
     * @param MenuBuilder|Item $menu
     * @param array $items
     * @return MenuBuilder|Item
     */
    private function buildFromArray($menu, array $items)
    {
        /** @var Gate $gate */
        $gate = resolve(Gate::class);

        foreach ($items as $item) {
            $can = array_pull($item, 'can');

            if ($can && $gate->denies($can)) {
                if ($menu instanceof Item) {
                    $menu->data('has-cannot-children', true);
                }
                continue;
            }

            $title = array_pull($item, 'title');
            $children = array_pull($item, 'children');

            /** @var Item $menuItem */
            $menuItem = $menu->add(__($title), $item);

            if ($children) {
                $this->buildFromArray($menuItem, $children);
            }
        }

        if ($menu instanceof MenuBuilder) {
            return $menu->filter(function(Item $item){
                if ($item->data('has-cannot-children') && !$item->hasChildren()) {
                    return false;
                }
                return true;
            });
        }

        return $menu;
    }
}
