<?php
/**
 * User: mike
 * Date: 6/14/2018
 * Time: 19:31
 */

namespace App\Forms;


use Collective\Html\FormBuilder;
use Kris\LaravelFormBuilder\Form;

class VueFormBuilder extends FormBuilder
{
    /**
     * Create a form input field.
     *
     * @param  string $type
     * @param  string $name
     * @param  string $value
     * @param  array  $options
     *
     * @return \Illuminate\Support\HtmlString
     */
    public function input($type, $name, $value = null, $options = [])
    {
        if ($type !== 'hidden') {
            $options[':error-messages'] = 'errors[\'' . e($name) . '\']';
        }

        if (in_array($type, ['checkboxs', 'radio', 'hidden'])) {
            return parent::input($type, $name, $value, $options);
        }

        $this->type = $type;

        if (! isset($options['name'])) {
            $options['name'] = $name;
        }

        // We will get the appropriate value for the given field. We will look for the
        // value in the session for the value in the old input data then we'll look
        // in the model instance if one is set. Otherwise we will just use empty.
        $id = $this->getIdAttribute($name, $options);

        if (! in_array($type, $this->skipValueTypes)) {
            $value = $this->getValueAttribute($name, $value);
        }

        // Once we have the type, value, and ID we can merge them into the rest of the
        // attributes array so we can convert them into their HTML attribute format
        // when creating the HTML element. Then, we will return the entire input.
        $merge = compact('type', 'value', 'id');

        $options = array_merge($options, $merge);

        $tag = 'v-text-field';

        if ($type === 'checkbox') {
            unset($options['value']);
            $tag = 'v-switch';
        }

        return $this->toHtmlString('<' . $tag . $this->html->attributes($options) . '></' . $tag . '>');
    }

    public function button($value = null, $options = [])
    {
        if (! array_key_exists('type', $options)) {
            $options['type'] = 'button';
        }

        if (array_key_exists('icon', $options)) {
            $options['title'] = $value;
            $value = '<v-icon>' . $options['icon'] . '</v-icon>';
            $options['icon'] = true;
        }

        return $this->toHtmlString('<v-btn' . $this->html->attributes($options) . '>' . $value . '</v-btn>');
    }

    public function checkbox($name, $value = 1, $checked = null, $options = [], $label = null)
    {
        if ($label) {
            $options['label'] = $label;
        }

        $this->type = 'checkbox';
        $options[':input-value'] = $this->getCheckedState('checkbox', $name, $value, $checked) ? 'true' : 'false';

        return parent::checkbox($name, $value, $checked, $options);
    }

    public function vLabel($name, $value = null, $options = [], $escape_html = true)
    {
        $this->labels[] = $name;

        $options = $this->html->attributes($options);

        $value = $this->formatLabel($name, $value);

        if ($escape_html) {
            $value = $this->html->entities($value);
        }

        return $this->toHtmlString('<v-label for="' . $name . '"' . $options . '>' . $value . '</v-label>');
    }

    public function textarea($name, $value = null, $options = [])
    {
        $this->type = 'textarea';

        if (! isset($options['name'])) {
            $options['name'] = $name;
        }

        // Next we will look for the rows and cols attributes, as each of these are put
        // on the textarea element definition. If they are not present, we will just
        // assume some sane default values for these attributes for the developer.
        $options = $this->setTextAreaSize($options);

        $options['id'] = $this->getIdAttribute($name, $options);

        $options['value'] = (string) $this->getValueAttribute($name, $value);

        unset($options['size']);

        // Next we will convert the attributes into a string form. Also we have removed
        // the size attribute, as it was merely a short-cut for the rows and cols on
        // the element. Then we'll create the final textarea elements HTML for us.
        $options = $this->html->attributes($options);

        return $this->toHtmlString('<v-textarea' . $options . '></v-textarea>');
    }
}
