<?php
/**
 * User: mike
 * Date: 6/14/2018
 * Time: 19:54
 */

namespace App\Forms;


use Illuminate\Support\Facades\Facade;

class VueFormFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'vue-form';
    }
}