<?php
/**
 * User: mike
 * Date: 10/17/2018
 * Time: 10:31
 */

namespace App\Forms\Admin;


use Kris\LaravelFormBuilder\Fields\ButtonType;

class FormAction extends ButtonType
{

    protected function getDefaults()
    {
        return [
            'wrapper' => false,
            'attr' => [
                'type' => 'button',
                'flat'
            ]
        ];
    }
}
