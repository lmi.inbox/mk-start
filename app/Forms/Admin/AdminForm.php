<?php
/**
 * User: mike
 * Date: 6/14/2018
 * Time: 18:45
 */

namespace App\Forms\Admin;


use Kris\LaravelFormBuilder\Form;

class AdminForm extends Form
{
    protected $templatePrefix = 'admin.form.';

    protected $formOptions = [
        'method' => 'POST',
        'url' => null,
        'v-admin-form',
        'class' => 'admin-form'
    ];

    /**
     * @var string
     */
    public $title;

    /**
     * @var string
     */
    public $saveText;

    /**
     * @var string
     */
    public $cancelText;

    /**
     * @var string
     */
    public $cancelUrl;

    /**
     * @var array
     */
    public $actions = ['save', 'save_and_close', 'save_and_new', 'cancel'];

    public function __construct()
    {
        $this->saveText = __('Save');
        $this->cancelText = __('Cancel');
    }


    public function renderPage($template = null, array $data = [])
    {
        if ($this->saveText && !$this->has('action.save')) {
            $this->add('action.save', 'admin.action', [
                'label' => $this->saveText,
                'attr' => [
                    'type' => 'submit',
                    'icon' => 'done',
                    'v-admin-form-submit'
                ]
            ]);
        }

        if ($this->cancelText && !$this->has('action.cancel')) {
            $this->add('action.cancel', 'admin.action', [
                'label' => $this->cancelText,
                'attr' => [
                    'v-admin-form-cancel',
                    'href' => $this->cancelUrl,
                    'icon' => 'undo'
                ]
            ]);
        }

        if (!$this->has('action.save_and_close')) {
            $this->add('action.save_and_close', 'admin.action', [
                'label' => __('Save & Close'),
                'attr' => [
                    'type' => 'submit',
                    'icon' => 'play_for_work',
                    'v-admin-form-submit' => 1
                ]
            ]);
        }

        if (!$this->has('action.save_and_new')) {
            $this->add('action.save_and_new', 'admin.action', [
                'label' => __('Save & New'),
                'attr' => [
                    'type' => 'submit',
                    'icon' => 'done_all',
                    'v-admin-form-submit' => 2
                ]
            ]);
        }

        $this->add('submit-flag', 'hidden');

        return view($template ?: 'admin.form.page', $data, ['form' => $this]);
    }

    public function setFormOptions(array $formOptions)
    {
        parent::setFormOptions($formOptions);

        $this->pullFromOptions('title', 'setTitle');
        $this->pullFromOptions('actions', 'setActions');
        $this->pullFromOptions('saveText', 'setSaveText');
        $this->pullFromOptions('cancelText', 'setCancelText');
        $this->pullFromOptions('cancelUrl', 'setCancelUrl');

        return $this;
    }

    public function setTitle(string $title)
    {
        $this->title = $title;

        return $this;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setActions(array $actions)
    {
        $this->actions = $actions;

        return $this;
    }

    public function getActions()
    {
        return $this->actions;
    }

    /**
     * @param string $saveText
     * @return AdminForm
     */
    public function setSaveText(string $saveText): AdminForm
    {
        $this->saveText = $saveText;
        return $this;
    }

    /**
     * @param string $cancelText
     * @return AdminForm
     */
    public function setCancelText(string $cancelText): AdminForm
    {
        $this->cancelText = $cancelText;
        return $this;
    }

    /**
     * @param string $cancelUrl
     * @return AdminForm
     */
    public function setCancelUrl(string $cancelUrl): AdminForm
    {
        $this->cancelUrl = $cancelUrl;
        return $this;
    }
}
