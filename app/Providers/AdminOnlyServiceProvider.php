<?php
/**
 * User: mike
 * Date: 09.05.18
 * Time: 11:10
 */

namespace App\Providers;


use App\Forms\VueFormBuilder;
use App\Forms\VueFormFacade;
use App\Services\SysInfo;
use Illuminate\Foundation\AliasLoader;
use Illuminate\Support\ServiceProvider;

class AdminOnlyServiceProvider extends ServiceProvider
{
    protected $providers = [
        AdminMenuServiceProvider::class
    ];

    public function register()
    {
        foreach ($this->providers as $provider) {
            $this->app->register($provider);
        }

        $this->app->singleton(SysInfo::class, function($app) {
            return new SysInfo($app['request'], $app['db']);
        });

        $this->app->singleton('vue-form', function ($app) {
            $form = new VueFormBuilder($app['html'], $app['url'], $app['view'], $app['session.store']->token(), $app['request']);

            return $form->setSessionStore($app['session.store']);
        });

        AliasLoader::getInstance()->alias(
            'Form',
            VueFormFacade::class
        );
    }
}
