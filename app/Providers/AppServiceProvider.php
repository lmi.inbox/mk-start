<?php

namespace App\Providers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //Prevent error 1071 in mysql < 5.7
        Schema::defaultStringLength(191);

        if ($this->app->environment() !== 'production') {
            $this->app->register(\MartinLindhe\VueInternationalizationGenerator\GeneratorProvider::class);
        }

        /** @var Request $request */
        $request = $this->app['request'];

        if ($this->app->runningInConsole()
            || ($request && starts_with($request->path(), config('admin.url', 'admin')))
        ) {
            $this->app->register(AdminOnlyServiceProvider::class);
        }
    }
}
