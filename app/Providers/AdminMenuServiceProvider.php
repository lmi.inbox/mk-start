<?php
/**
 * User: mike
 * Date: 09.05.18
 * Time: 11:13
 */

namespace App\Providers;


use App\Services\AdminMenu;
use App\ServiceTag;
use Illuminate\Support\ServiceProvider;

class AdminMenuServiceProvider extends ServiceProvider
{
    protected $defer = true;

    public function register()
    {
        $this->app->singleton(AdminMenu::class, function($app) {
            return new AdminMenu($app['config']->get('admin.menu', []), $app->tagged(ServiceTag::ADMIN_MENU));
        });
    }

    public function provides()
    {
        return [
            AdminMenu::class
        ];
    }
}
