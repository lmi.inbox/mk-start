<?php
/**
 * User: mike
 * Date: 10/19/2018
 * Time: 11:35
 */

namespace App\Providers;


use App\Services\HtmlBuilder;

class HtmlServiceProvider extends \Collective\Html\HtmlServiceProvider
{
    /**
     * Register the HTML builder instance.
     *
     * @return void
     */
    protected function registerHtmlBuilder()
    {
        $this->app->singleton('html', function ($app) {
            return new HtmlBuilder($app['url'], $app['view']);
        });
    }
}
