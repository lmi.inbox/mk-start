<?php
/**
 * User: mike
 * Date: 09.05.18
 * Time: 11:19
 */

namespace App\Contracts;


interface IMenuManager
{
    /**
     * @return array
     */
    public function items(): array;
}
