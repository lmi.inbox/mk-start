<?php
/**
 * User: mike
 * Date: 09.05.18
 * Time: 11:18
 */

namespace App\Contracts;


use Lavary\Menu\Builder as MenuBuilder;

interface IAdminMenuBuilder
{
    /**
     * @param MenuBuilder $menu
     */
    public function build(MenuBuilder $menu);
}
