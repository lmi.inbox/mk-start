<?php

namespace App\Http\Middleware\Admin;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class AfterMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);

        if ($request->ajax() || $request->wantsJson()) {
            // Process flash messages
            $this->processFlashMessages($request, $response);
        }

        return $response;
    }

    /**
     * @param Request $request
     * @param Response $response
     */
    private function processFlashMessages($request, $response)
    {
        $types = ['success', 'info', 'error', 'warning'];

        foreach ($types as $type) {
            $message = $request->session()->get($type);

            if ($message) {
                $this->setFlashMessageHeader($request, $response, $message, $type);
            }
        }
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param string $message
     * @param string $type
     *
     * @return Response
     */
    private function setFlashMessageHeader($request, $response, string $message, string $type = 'info')
    {
        $response->headers->set('X-Flash-Message', rawurlencode($message));
        $response->headers->set('X-Flash-Message-Type', $type);
        $request->session()->forget($type);

        return $response;
    }
}
