<?php

namespace App\Http\Middleware\Admin;

use Closure;

class AdminLocale
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->has('_lang') && ($lang = $request->get('_lang'))) {
            $to = route('admin.home');

            if ($path = $request->get('_path')) {
                $to .= $path;
            }

            return redirect($to)->cookie(\Cookie::forever('admin_lc', $lang));
        }

        app()->setLocale($request->cookie('admin_lc', config('admin.locale')));

        return $next($request);
    }
}
