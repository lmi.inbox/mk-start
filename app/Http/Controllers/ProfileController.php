<?php

namespace App\Http\Controllers;

use App\Rules\CurrentPassword;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;

class ProfileController extends Controller
{
    public function form(Request $request)
    {
        return view('profile.form', ['user' => $request->user()]);
    }

    public function password()
    {
        return view('profile.password');
    }

    public function update(Request $request)
    {
        /** @var User $user */
        $user = $request->user();

        $this->validate($request, [
            'name' => 'required|string|max:255',
            'email' => [
                'required',
                'string',
                'email',
                'max:255',
                Rule::unique('users')->ignore($user->id)
            ]
        ]);

        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->save();

        return redirect()->back()->with('status', 'Your profile has been updated');
    }

    public function updatePassword(Request $request)
    {
        $this->validate($request, [
            'current_password' => [
                'required',
                new CurrentPassword()
            ],
            'password' => 'required|string|min:6|confirmed',
        ]);

        $user = $request->user();
        $user->password = Hash::make($request->input('password'));
        $user->save();

        return redirect()->back()->with('status', 'Your password has been updated');
    }
}
