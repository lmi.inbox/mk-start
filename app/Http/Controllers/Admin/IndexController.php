<?php

namespace App\Http\Controllers\Admin;

use App\Services\AdminMenu;
use Illuminate\Http\Request;

class IndexController extends AdminController
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            return redirect()->route('admin.dashboard');
        }

        return view('admin.app');
    }

    public function menu(AdminMenu $menu)
    {
		return $menu->items();
    }
}
