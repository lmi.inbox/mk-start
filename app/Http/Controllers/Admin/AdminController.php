<?php
/**
 * User: mike
 * Date: 10.11.17
 * Time: 14:31
 */

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Request;

class AdminController extends Controller
{
    public function redirectUrl($url)
    {
        if (is_array($url)) {
            $url = $this->matchRedirectUrl($url);
        }

        return response()->json(compact('url'), 302);
    }

    public function redirectRoute($route, $params = [], $hash = true): RedirectResponse
    {
        if (is_array($route)) {
            $route = $this->matchRedirectUrl($route);
        }

        $url = route($route, [], !$hash);

        return $this->redirectUrl($url);
    }

    private function matchRedirectUrl(array $urls)
    {
        $submitFlag = (int) Request::header('X-Submit-Flag', 0);

        return array_key_exists($submitFlag, $urls) ? $urls[$submitFlag] : array_first($urls);
    }
}
