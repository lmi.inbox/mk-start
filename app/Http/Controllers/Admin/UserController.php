<?php

namespace App\Http\Controllers\Admin;


use App\DataGrid\DataGrid;
use App\DataGrid\DataGridView;
use App\Forms\Admin\UserForm;
use App\Traits\DeleteRequests;
use App\Traits\HasModel;
use App\Traits\ToggleRequests;
use App\User;
use Illuminate\Auth\Events\Verified;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Kris\LaravelFormBuilder\FormBuilder;

class UserController extends AdminController
{
    use HasModel,
        ToggleRequests,
        DeleteRequests;

    protected $modelClass = User::class;

    public function index()
    {
        return new DataGridView(
            __('Users'),
            route('admin.user.data'),
            [
                'addUrl' => 'user/create',
                'actions' => [
                    'verify' => 'user/verify',
                    'edit' => 'user/edit',
                    'delete' => route('admin.user.delete')
                ],
                'menu' => [
                    'activate' => route('admin.user.activate'),
                    'deactivate' => route('admin.user.deactivate'),
                    'delete' => route('admin.user.delete'),
                ]
            ]
        );
    }

    public function data(DataGrid $dataGrid)
    {
        $items = User::select([
            'id',
            'name',
            'email',
            'active',
            'is_admin',
            'email_verified_at',
            'created_at'
        ]);

        $headers = [
            ['text' => 'ID', 'value' => 'id', 'align' => 'center'],
            ['text' => __('Name'), 'value' => 'name', 'type' => 'link', 'url' => '#/user/edit/{id}'],
            ['text' => 'E-Mail', 'value' => 'email'],
            [
                'text' => __('Status'), 'value' => 'active', 'type' => 'toggle',
                'url' => route('admin.user.toggle'),
                'except' => [auth()->id()]
            ],
            [
                'text' => __('Administrator'), 'value' => 'is_admin', 'type' => 'boolean', 'align' => 'center'
            ],
            [
                'text' => __('Verified'), 'value' => 'email_verified_at', 'type' => 'verify',
            ],
        ];

        return $dataGrid->make($items)
            ->setHeaders($headers)
            ->setSearchColumns(['users.name', 'users.email'])
            ->setParam('itemExpressions', [
                'rowClass' => '{"error lighten-5": !item.active, "success lighten-5": item.is_admin, "warning lighten-4": !item.email_verified_at}',
                'disableDelete' => 'item.id===' . auth()->id(),
                'disableVerify' => '!!item.email_verified_at'
            ])
            ->apply()
            ->response();
    }

    public function toggleable($model = null): Builder
    {
        return User::notCurrent();
    }

    public function deletable(): Builder
    {
        return User::notCurrent();
    }

    public function create(FormBuilder $formBuilder)
    {
        $form = $formBuilder->create(UserForm::class, [
            'url' => route('admin.user.store'),
            'title' => 'Create user',
            'cancelUrl' => route('admin.user.index')
        ]);

        return $form->renderPage();
    }

    public function edit(User $model, FormBuilder $formBuilder)
    {
        $form = $formBuilder->create(UserForm::class, [
            'url' => route('admin.user.update', $model),
            'model' => $model,
            'title' => 'Edit user',
            'cancelUrl' => route('admin.user.index')
        ]);

        return $form->renderPage();
    }

    public function store(User $model, Request $request)
    {
        $this->save($model, $request);

        $request->session()->flash('success', 'User created!');

        return $this->redirectUrl([
            '/user/edit/' . $model->id,
            '/user',
            '/user/create'
        ]);
    }

    public function update(User $model, Request $request)
    {
        $this->save($model, $request);

        $request->session()->flash('success', 'User updated!');

        return $this->redirectUrl([
            '/user/edit/' . $model->id,
            '/user',
            '/user/create'
        ]);
    }

    private function save(User $model, Request $request)
    {
        $id = $model->id;

        $this->validate($request, [
            'email' => 'required|email|unique:users,email' . ($id ? ','.$id : ''),
            'name' => 'required',
            'password' => 'confirmed' . (!$id ? '|required' : '')
        ]);

        $model->email = $request->input('email');
        $model->name = $request->input('name');

        if ($password = $request->input('password')) {
            $model->password = bcrypt($password);
        }

        if (!$model->isCurrent()) {
            $model->active = $request->input('active', false);
        }

        return $model->save();
    }

    public function verify(Request $request, User $user = null) {
        if (!$user) {
            $id = $request->input('id');

            if (!$id || !($user = User::find($id))) {
                abort(404);
            }

            if ($user->hasVerifiedEmail()) {
                abort(422);
            }

            $user->markEmailAsVerified();
        }

        return ['item' => $user];
    }
}
