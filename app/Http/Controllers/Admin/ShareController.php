<?php

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;

class ShareController extends Controller
{
    public function config()
    {
        $config = [
            'language' => app()->getLocale(),
            'languages' => config('admin.locales', [])
        ];

        return $config;
    }
}
