<?php

namespace App\Http\Controllers\Admin;

use App\Services\SysInfo;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SystemController extends AdminController
{
    /**
     * @var SysInfo
     */
    private $sysInfo;

    public function __construct(SysInfo $sysInfo)
    {
        $this->sysInfo = $sysInfo;
    }

    public function info()
    {
        return response()->view('admin.system.info', $this->infoData())
            ->header('Model-Data-Url', route('admin.info.json'));
    }

    /**
     * @return array
     */
    public function infoData()
    {
        $data = [
            'system' => $this->sysInfo->getSystemInfo(),
            'mysql' => $this->sysInfo->getMysqlInfo(),
            'php' => [
                'version' => $this->sysInfo->getPhpVersion(),
                'built_on' => $this->sysInfo->getPhpBuiltOn(),
                'sapi' => $this->sysInfo->getPhpSapi(),
                'settings' => $this->sysInfo->getPhpSettings(),
                'extensions' => $this->sysInfo->getPhpExtensions(),
                'info_url' => route('admin.info.php')
            ],
            'vars' => [
                'server' => $this->sysInfo->getServerVariables(),
                'env' => $this->sysInfo->getEnv()
            ]
        ];

        if ($this->sysInfo->isApache()) {
            $data['apache'] = [
                'version' => $this->sysInfo->getApacheVersion(),
                'modules' => $this->sysInfo->getApacheModules()
            ];
        }

        return $data;
    }

    public function infoPhp()
    {
        ob_start();
        date_default_timezone_set('UTC');
        phpinfo();
        $phpinfo = ob_get_contents();
        ob_end_clean();

        return $phpinfo;
    }
}
