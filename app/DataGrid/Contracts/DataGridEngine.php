<?php
/**
 * User: mike
 * Date: 22.11.17
 * Time: 17:11
 */

namespace App\DataGrid\Contracts;


use Illuminate\Http\JsonResponse;

interface DataGridEngine
{
    /**
     * @return mixed
     */
    public function results();

    /**
     * Get rows count
     *
     * @return int
     */
    public function count();

    /**
     * Perform global search
     */
    public function search();

    /**
     * Perform column filtering
     */
    public function filtering();

    /**
     * Perform ordering
     */
    public function ordering();

    /**
     * Perform pagination
     */
    public function paging();

    /**
     * Set columns for global search;
     *
     * @param array $columns
     * @return DataGridEngine
     */
    public function setSearchColumns(array $columns);

    /**
     * @param array $columns
     * @return DataGridEngine
     */
    public function setHavingColumns(array $columns);

    /**
     * @param array $headers
     * @return DataGridEngine
     */
    public function setHeaders(array $headers);

    /**
     * @param array $actions
     * @return DataGridEngine
     */
    public function setActions(array $actions);

    /**
     * Apply filters, orders
     * @return DataGridEngine
     */
    public function apply();

    /**
     * Return json response of filtered and ordered results
     *
     * @return JsonResponse
     */
    public function response();
}