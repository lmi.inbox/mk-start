<?php
/**
 * User: mike
 * Date: 22.11.17
 * Time: 17:08
 */

namespace App\DataGrid;


use Illuminate\Contracts\Foundation\Application;
use Illuminate\Support\ServiceProvider;

class DataGridServiceProvider extends ServiceProvider
{
    protected $defer = true;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(DataGrid::class, function(Application $app) {
            return new DataGrid($app->make(Request::class));
        });
    }

    public function provides()
    {
        return [
            DataGrid::class
        ];
    }
}
{

}