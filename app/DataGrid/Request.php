<?php
/**
 * User: mike
 * Date: 22.11.17
 * Time: 17:00
 */

namespace App\DataGrid;


class Request extends \Illuminate\Http\Request
{
    /**
     * Get columns ordering
     *
     * @return array
     */
    public function getOrder()
    {
        $predicate = $this->input('sort.by');

        if (!$predicate) {
            return [];
        }

        $reverse = $this->input('sort.reverse', false);

        if ($reverse === 'false' || $reverse === "0") {
            $reverse = false;
        }

        return [$predicate => $reverse];
    }

    /**
     * Get page
     *
     * @return int
     */
    public function getPage()
    {
        $result = $this->input('page', 1);

        return $result > 0 ? $result : 1;
    }

    /**
     * Get count per page
     *
     * @return int
     */
    public function getCount()
    {
        $count = (int) $this->input('perPage', 10);
        return $count > 0 ? $count : 10;
    }

    /**
     * Get start pagination
     *
     * @return int
     */
    public function getStart()
    {
        return $this->getCount() * ($this->getPage() - 1);
    }

    /**
     * Get global search
     *
     * @return mixed
     */
    public function getSearch()
    {
        return $this->input('search');
    }

    /**
     * @return array|string
     */
    public function getFilter()
    {
        return $this->input('filter');
    }

    /**
     * @return bool
     */
    public function withHeaders(): bool {
        $result = $this->input('withHeaders', false);

        if ($result === 'false' || $result === "0") {
            $result = false;
        }

        return $result;
    }

    public function isJson()
    {
        return $this->getRealMethod() === 'GET' || parent::isJson();
    }
}
