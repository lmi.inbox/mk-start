<?php
/**
 * User: mike
 * Date: 22.11.17
 * Time: 17:12
 */

namespace App\DataGrid\Engines;


use App\DataGrid\Contracts\DataGridEngine;
use App\DataGrid\Request;
use Illuminate\Http\JsonResponse;

abstract class BaseEngine implements DataGridEngine
{
    /**
     * @var Request
     */
    protected $request;

    /**
     * @var bool
     */
    protected $applied = false;

    /**
     * @var array
     */
    protected $searchColumns = [];

    /**
     * @var array
     */
    protected $havingColumns = [];

    /**
     * @var array
     */
    protected $headers = [];

    /**
     * @var array
     */
    protected $actions = [];

    /**
     * @var array
     */
    protected $params = [];

    /**
     * Set columns for global search;
     *
     * @param array $columns
     * @return $this
     */
    public function setSearchColumns(array $columns)
    {
        $this->searchColumns = $columns;

        return $this;
    }

    /**
     * @param array $columns
     * @return $this
     */
    public function setHavingColumns(array $columns)
    {
        $this->havingColumns = $columns;

        return $this;
    }

    /**
     * @param array $headers
     * @return $this
     */
    public function setHeaders(array $headers)
    {
        $this->headers = $headers;

        return $this;
    }

    /**
     * @param array $actions
     * @return $this
     */
    public function setActions(array $actions)
    {
        $this->actions = $actions;

        return $this;
    }

    /**
     * @param string $key
     * @param $value
     * @return $this
     */
    public function setParam(string $key, $value)
    {
        $this->params[$key] = $value;

        return $this;
    }

    /**
     * @param string $key
     * @return $this
     */
    public function removeParam(string $key)
    {
        unset($this->params[$key]);

        return $this;
    }

    /**
     * @param array $params
     * @return $this
     */
    public function setParams(array $params)
    {
        $this->params = $params;

        return $this;
    }

    /**
     * Apply filters, orders
     * @return DataGridEngine
     */
    public function apply()
    {
        if (!$this->applied) {
            $this->search();
            $this->filtering();
            $this->ordering();

            $this->applied = true;
        }

        return $this;
    }

    /**
     * Return json response of filtered and ordered results
     *
     * @return JsonResponse
     */
    public function response()
    {
        $this->apply();

        $count = $this->count();
        $pages = ceil($count / $this->request->getCount());
        $this->paging();

        $result = [
            'items' => $this->results(),
            'pages' => $pages,
            'total' => $count
        ];

        if ($this->request->withHeaders()) {
            $result['headers'] = $this->headers;
            $result['actions'] = $this->actions;
            $result['params'] = $this->params;
        }

        return response()->json($result);
    }
}
