<?php
/**
 * User: mike
 * Date: 22.11.17
 * Time: 17:19
 */

namespace App\DataGrid\Engines;


use App\DataGrid\Contracts\DataGridEngine;
use App\DataGrid\Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder as EloquentBuilder;

class EloquentEngine extends QueryBuilderEngine implements DataGridEngine
{
    /**
     * @var Model
     */
    protected $model;

    public function __construct($model, Request $request)
    {
        $this->request = $request;
        $this->query = $model instanceof EloquentBuilder ? $model->getQuery() : $model;
        $this->model = $model;
        $this->columns = $this->query->columns;
        $this->prefix = $this->query->getGrammar()->getTablePrefix();
    }

    /**
     * @return mixed
     */
    public function results()
    {
        return $this->model->get()->toArray();
    }
}