<?php
/**
 * User: mike
 * Date: 22.11.17
 * Time: 17:14
 */

namespace App\DataGrid\Engines;


use App\DataGrid\Contracts\DataGridEngine;
use App\DataGrid\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;

class CollectionEngine extends BaseEngine implements DataGridEngine
{
    /**
     * @var Collection
     */
    private $collection;

    /**
     * CollectionEngine constructor.
     * @param Collection $collection
     * @param Request $request
     */
    public function __construct(Collection $collection, Request $request)
    {
        $this->collection = $collection;
        $this->request = $request;
    }

    /**
     * @return mixed
     */
    public function results()
    {
        return $this->collection->all();
    }

    /**
     * Get rows count
     *
     * @return int
     */
    public function count()
    {
        return $this->collection->count();
    }

    /**
     * Perform global search
     */
    public function search()
    {
        $search = $this->request->getSearch();

        if ($search && $this->searchColumns) {
            $this->filterColumns(array_fill_keys($this->searchColumns, $search));
        }
    }

    protected function filterColumns($filters)
    {
        foreach($filters as $key=>$filter) {
            $this->collection = $this->collection->filter(
                function ($row) use ($key, $filter) {
                    return strpos(Str::lower($row[$key]), Str::lower($filter)) !== false;
                }
            );
        }
    }

    /**
     * Perform column filtering
     */
    public function filtering()
    {
        $this->filterColumns($this->request->getFilter());
    }

    /**
     * Perform ordering
     */
    public function ordering()
    {
        foreach ($this->request->getOrder() as $key=>$reverse) {
            $this->collection = $this->collection->sortBy(
                function ($row) use ($key) {
                    return $row[$key];
                }
                , SORT_NATURAL, $reverse);
        }
    }

    /**
     * Perform pagination
     */
    public function paging()
    {
        $this->collection = $this->collection->slice(
            $this->request->getStart(),
            $this->request->getCount()
        );
    }

    public function __call($name, $arguments)
    {
        if ($name == 'orderBy') {
            $name = 'sortBy';
        }

        call_user_func_array([$this->collection, $name], $arguments);

        return $this;
    }
}