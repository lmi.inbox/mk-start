<?php
/**
 * User: mike
 * Date: 22.11.17
 * Time: 16:59
 */

namespace App\DataGrid;


use App\DataGrid\Engines\CollectionEngine;
use App\DataGrid\Engines\EloquentEngine;
use App\DataGrid\Engines\QueryBuilderEngine;
use Illuminate\Support\Collection;
use Illuminate\Database\Query\Builder as QueryBuilder;
use Illuminate\Database\Eloquent\Builder as EloquentBuilder;

class DataGrid
{
    /**
     * @var Request
     */
    protected $request;
    /**
     * SmartTable constructor.
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request->request->count() ? $request : Request::capture();
    }

    /**
     * @param Collection|QueryBuilder|EloquentBuilder $builder
     * @return CollectionEngine|EloquentEngine|QueryBuilderEngine
     */
    public static function make($builder)
    {
        /** @var DataGrid $self */
        $self = app(self::class);
        if ($builder instanceof QueryBuilder) {
            return new QueryBuilderEngine($builder, $self->request);
        }
        if ($builder instanceof EloquentBuilder) {
            return new EloquentEngine($builder, $self->request);
        }
        if ($builder instanceof Collection) {
            return new CollectionEngine($builder, $self->request);
        }
        throw new \InvalidArgumentException("'{$builder}' must be instance of Collection or QueryBuilder or EloquentBuilder");
    }
}