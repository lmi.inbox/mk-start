<?php
/**
 * User: mike
 * Date: 5/14/2018
 * Time: 10:56
 */

namespace App\DataGrid;


/**
 * Class DataGridView
 * @package App\DataGrid
 */
class DataGridView
{
    /**
     * @var string
     */
    public $title;

    /**
     * @var string
     */
    public $url;

    /**
     * @var string
     */
    public $addUrl;

    /**
     * @var string
     */
    public $idKey = 'id';

    /**
     * @var array
     */
    public $actions = [];

    /**
     * @var array
     */
    public $menu = [];

    /**
     * @var bool
     */
    public $selectable = true;

    /**
     * @var bool
     */
    public $searchable = true;

    /**
     * DataGridView constructor.
     * @param string $title
     * @param string $url
     * @param array $options
     */
    public function __construct(string $title, string $url, array $options = [])
    {
        $this->title = $title;
        $this->url = $url;
        $this->setOptions($options);
    }

    /**
     * @param array $options
     * @return $this
     */
    public function setOptions(array $options)
    {
        foreach ($options as $key => $value) {
            if (property_exists($this, $key)) {
                $this->$key = $value;
            }
        }

        return $this;
    }

    /**
     * @return array
     */
    private function data(): array
    {
        $data = get_object_vars($this);

        $data['hasActions'] = $data['actions'] ? true : false;

        return $data;
    }

    /**
     * @param string|null $path
     * @param array $data
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function view(string $path = null, array $data = [])
    {
        return view($path ?: 'admin.datagrid.index', $this->data(), $data);
    }

    /**
     * @return string
     * @throws \Throwable
     */
    public function __toString()
    {
        return $this->view()->render();
    }
}