<?php

namespace App;

use App\Traits\HasActive;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * Class User
 * @package App
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $password
 * @property boolean $is_admin
 */
class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable,
        HasActive;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'is_admin' => 'bool',
        'created_at' => 'datetime:Y-m-d H:i:s',
        'updated_at' => 'datetime:Y-m-d H:i:s',
        'email_verified_at' => 'datetime:Y-m-d H:i:s',
    ];

    public function isAdmin(): bool {
        return $this->getAttribute('is_admin');
    }

	/**
     * @param Builder $query
     * @return Builder
     */
    public function scopeNotCurrent(Builder $query)
    {
        return $query->where('id', '!=', auth()->id());
    }

    /**
     * @return bool
     */
    public function getIsCurrentAttribute()
    {
        return isset($this->attributes['id']) && $this->attributes['id'] == auth()->id();
    }

    /**
     * @return bool
     */
    public function isCurrent()
    {
        return $this->getIsCurrentAttribute();
    }
}
